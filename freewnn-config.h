/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#ifndef _FREEWNN_CONFIG_H
#define _FREEWNN_CONFIG_H

#include <gtk/gtk.h>

#include "keytable.h"

typedef struct _FreeWnnConfig FreeWnnConfig;

FreeWnnConfig* freewnn_config_new(FreeWnnFuncTable** functablep);
void freewnn_config_run(FreeWnnConfig* config);
void freewnn_config_finalize(FreeWnnConfig* config);

#endif /* _FREEWNN_CONFIG_H */
