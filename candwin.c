/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "candwin.h"

#define MARGIN_X 5
#define MARGIN_Y 5

/* Calculate Positions */
static void
calculate(GtkWidget* widget, CandWin* candwin) {
  const int window_maxwidth = 400;
  int width = 0, height = 0;
  int string_maxwidth = 0;
  int i=0;

  PangoLayout* layout = gtk_widget_create_pango_layout(widget, "あ");
  pango_layout_get_pixel_size(layout, &width, &height);

  candwin->line_height = height;

  for(i=0;i<g_slist_length(candwin->cand_slist);i++) {
    gchar* text = g_slist_nth_data(candwin->cand_slist, i);
    pango_layout_set_text(layout, text, -1);
    pango_layout_get_pixel_size(layout, &width, &height);
    if( width > string_maxwidth ) {
      string_maxwidth = width;
    }
  }
  candwin->string_maxwidth = string_maxwidth;

  for(i=0;(string_maxwidth*(i+1) + MARGIN_X*(i+2))<window_maxwidth;i++);

  candwin->num_in_aline = i;
  candwin->window_width = string_maxwidth*i + MARGIN_X*(i+1);

  candwin->num_of_lines = g_slist_length(candwin->cand_slist) / i + ((g_slist_length(candwin->cand_slist) % i) == 0 ? 0 : 1 );
  candwin->window_height = candwin->line_height * candwin->num_of_lines + MARGIN_Y * (candwin->num_of_lines + 1);

  g_object_unref(layout);
}

static void
scroll_cb(GtkWidget* widget, GdkEventScroll* event, CandWin* candwin) {
  switch(event->direction) {
  case GDK_SCROLL_UP:
    if( candwin->num_of_lines == 1 ) {
      candwin_markup_left(candwin); 
    } else {
      candwin_markup_up(candwin);
    }
    break;
  case GDK_SCROLL_DOWN:
    if( candwin->num_of_lines == 1 ) {
      candwin_markup_right(candwin); 
    } else {
      candwin_markup_down(candwin);
    }
    break;
  default:
    break;
  }
}

static gboolean
delete_cb(GtkWidget* widget, GdkEventAny* event, CandWin* candwin) {
  candwin->markup_index = candwin->init_index;
  
  gtk_widget_hide_all(candwin->window);

  return TRUE;
}

static void
expose_cb(GtkWidget* widget, GdkEventKey* event, CandWin* candwin) {
  guint i = 0;
  gint linenum = 0;
  gint xoffset = 0;
  gint yoffset = 0;

  if( candwin->string_maxwidth == 0 ) {
    calculate(widget, candwin);
    gtk_window_set_default_size(GTK_WINDOW(widget), candwin->window_width, candwin->window_height);
    gtk_window_resize(GTK_WINDOW(widget), candwin->window_width, candwin->window_height);
    gtk_widget_queue_resize(widget);
    return;
  }

  /* Change the style */
  /* This should happen in expose_cb to apply changing on
     gnome-theme-manager just in time.
   */
  gtk_widget_set_style(widget, gtk_rc_get_style_by_paths(gtk_widget_get_settings(widget), NULL, NULL, GTK_TYPE_BUTTON));

  xoffset = MARGIN_X;
  yoffset = MARGIN_Y;
  linenum = 0;
  for(i=0;i<g_slist_length(candwin->cand_slist);i++) {
    gchar* text = g_slist_nth_data(candwin->cand_slist, i);
    PangoLayout* layout = gtk_widget_create_pango_layout(widget, text);
    PangoAttrList *attrs = pango_attr_list_new();
    PangoAttribute *attr = NULL;

    GdkColor fg;

    fg = widget->style->fg[GTK_STATE_NORMAL];
    attr = pango_attr_foreground_new(fg.red, fg.green, fg.blue);
    attr->start_index = 0;
    attr->end_index = strlen(text);
    pango_attr_list_insert(attrs, attr);
    pango_layout_set_attributes(layout, attrs);
#if 0
    if( i == candwin->markup_index ) {
      PangoAttribute *attr = pango_attr_background_new(0, 0, 0);
      attr->start_index = 0;
      attr->end_index = strlen(text);
      pango_attr_list_insert(attrs, attr);
      attr = pango_attr_foreground_new(0xffff, 0xffff, 0xffff);
      attr->start_index = 0;
      attr->end_index = strlen(text);
      pango_attr_list_insert(attrs, attr);
      pango_layout_set_attributes(layout, attrs);
    }
#endif
    xoffset = MARGIN_X + (i % candwin->num_in_aline) * (candwin->string_maxwidth+MARGIN_X);
    if( i > 0 && (i % candwin->num_in_aline) == 0 ) {
      linenum++;
    }

    if( i==candwin->markup_index ) { /* Use Gtk theme for GtkButton */
      GdkRectangle area;
      int width, height;
      area.x = 0;
      area.y = 0;
      area.width = widget->allocation.width;
      area.height = widget->allocation.height;

    fg = widget->style->fg[GTK_STATE_PRELIGHT];
    attr = pango_attr_foreground_new(fg.red, fg.green, fg.blue);
    attr->start_index = 0;
    attr->end_index = strlen(text);
    pango_attr_list_insert(attrs, attr);
    pango_layout_set_attributes(layout, attrs);

      pango_layout_get_pixel_size(layout, &width, &height);
      gtk_paint_box(widget->style, widget->window, GTK_STATE_PRELIGHT, GTK_SHADOW_OUT, &area, widget, "button", xoffset-MARGIN_X, (candwin->line_height+MARGIN_Y)*linenum, width+MARGIN_X*2, height+MARGIN_Y*2);
    }

    gdk_draw_layout(widget->window, widget->style->black_gc, xoffset, MARGIN_Y + (candwin->line_height+MARGIN_Y)*linenum, layout);
    g_object_unref(layout);
  }

}

void
candwin_markup_up(CandWin* candwin) {
  if( candwin->markup_index >= candwin->num_in_aline ) {
    candwin->markup_index -= candwin->num_in_aline;
  } else {
    candwin->markup_index %= candwin->num_in_aline;
    candwin->markup_index += candwin->num_in_aline * candwin->num_of_lines;
    while( candwin->markup_index >= g_slist_length(candwin->cand_slist) ) {
      candwin->markup_index -= candwin->num_in_aline;
    }
  }

  gtk_widget_queue_draw(candwin->window);
}

void
candwin_markup_down(CandWin* candwin) {
  if( candwin->markup_index + candwin->num_in_aline < g_slist_length(candwin->cand_slist) ) {
    candwin->markup_index += candwin->num_in_aline;
  } else {
    candwin->markup_index %= candwin->num_in_aline;
  }

  gtk_widget_queue_draw(candwin->window);
}

void
candwin_markup_right(CandWin* candwin) {
  candwin->markup_index++;
  if( candwin->markup_index >= g_slist_length(candwin->cand_slist) ) {
    candwin->markup_index = 0;
  }

  gtk_widget_queue_draw(candwin->window);
}

void
candwin_markup_left(CandWin* candwin) {
  if( candwin->markup_index > 0 ) {
    candwin->markup_index--;
  } else {
    candwin->markup_index = g_slist_length(candwin->cand_slist)-1;
  }

  gtk_widget_queue_draw(candwin->window);
}

void
candwin_markup_linehead(CandWin* candwin) {
  candwin->markup_index -= ( candwin->markup_index % candwin->num_in_aline );

  gtk_widget_queue_draw(candwin->window);
}

void
candwin_markup_lineend(CandWin* candwin) {
  guint cand_num = g_slist_length(candwin->cand_slist);
  candwin->markup_index -= ( candwin->markup_index % candwin->num_in_aline );
  candwin->markup_index += candwin->num_in_aline - 1;

  if( candwin->markup_index >= cand_num )
    candwin->markup_index = cand_num - 1;

  gtk_widget_queue_draw(candwin->window);
}

gboolean
key_press(GtkWidget* widget, GdkEventKey* event, CandWin* candwin) {
  if( event->keyval == GDK_Escape ) {
    candwin->markup_index = candwin->init_index;
    gtk_widget_hide_all(candwin->window);
    return TRUE;
  }

//g_print("Keypress: 0x%02X\n", event->keyval);

  if( event->state & GDK_CONTROL_MASK ) {
    switch(event->keyval) {
    case GDK_p: /* Emacs key bindings */
      candwin_markup_up(candwin);
      break;
    case GDK_n: /* Emacs key bindings */
      candwin_markup_down(candwin);
      break;
    case GDK_f: /* Emacs key bindings */
      candwin_markup_right(candwin);
      break;
    case GDK_b: /* Emacs key bindings */
      candwin_markup_left(candwin);
      break;
    case GDK_a: /* Emacs key bindings */
      candwin_markup_linehead(candwin);
      break;
    case GDK_e: /* Emacs key bindings */
      candwin_markup_lineend(candwin);
      break;
    default:
      if( candwin->extra_filter_keypress ) {
        candwin->extra_filter_keypress(event, candwin->keyfunc_data);
      }
      break;
    }

    return TRUE;
  }

  switch(event->keyval) {
  case GDK_space:
  case GDK_Right:
  case GDK_l: /* Vi key bindings */
    candwin_markup_right(candwin);
    break;
  case GDK_Left:
  case GDK_h: /* Vi key bindings */
    candwin_markup_left(candwin);
    break;
  case GDK_Up:
  case GDK_k: /* Vi key bindings */
    candwin_markup_up(candwin);
    break;
  case GDK_Down:
  case GDK_j: /* Vi key bindings */
    candwin_markup_down(candwin);
    break;
  case GDK_asciicircum: /* Vi key bindings */
    candwin_markup_linehead(candwin);
    break;
  case GDK_dollar: /* Vi key bindings */
    candwin_markup_lineend(candwin);
    break;
  case GDK_y: /* NetHack key bindings */
    candwin_markup_up(candwin);
    candwin_markup_left(candwin);
    break;
  case GDK_u: /* NetHack key bindings */
    candwin_markup_up(candwin);
    candwin_markup_right(candwin);
    break;
  case GDK_b: /* NetHack key bindings */
    candwin_markup_down(candwin);
    candwin_markup_left(candwin);
    break;
  case GDK_n: /* NetHack key bindings */
    candwin_markup_down(candwin);
    candwin_markup_right(candwin);
    break;
  case GDK_Return:
    gtk_widget_hide_all(candwin->window);
  default:
    if( candwin->extra_filter_keypress ) {
      candwin->extra_filter_keypress(event, candwin->keyfunc_data);
    }
    break;
  }

  return TRUE;
}

static void
click_cb(GtkWidget* widget, GdkEventButton* event, CandWin* candwin) {
  gint line = (gint)event->y / (candwin->line_height + MARGIN_Y);
  gint x = (gint)event->x / (candwin->string_maxwidth + MARGIN_X);
  guint newindex = 0;

  if( line >= candwin->num_of_lines ) {
    line = candwin->num_of_lines - 1;
  }
  if( x >= candwin->num_in_aline ) {
    x = candwin->num_in_aline - 1;
  }

  newindex = candwin->num_in_aline * line + x;

  if( newindex >= g_slist_length(candwin->cand_slist) ) {
    newindex = candwin->markup_index;
  }
  candwin->markup_index = newindex;

  gtk_widget_queue_draw(widget);
  gtk_widget_hide_all(widget);
}

CandWin*
candwin_new() {
  CandWin* result = g_new0(CandWin, 1);
  result->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_modal(GTK_WINDOW(result->window), TRUE);
  gtk_window_set_title(GTK_WINDOW(result->window), _("Candidate Window"));
  gtk_window_set_default_size(GTK_WINDOW(result->window), 400, 20);
  gtk_widget_add_events(result->window, GDK_BUTTON_PRESS_MASK);
  g_signal_connect(result->window, "key_press_event", G_CALLBACK(key_press), result);
  g_signal_connect(result->window, "expose_event", G_CALLBACK(expose_cb), result);
  g_signal_connect(result->window, "button_press_event", G_CALLBACK(click_cb), result);
  g_signal_connect(result->window, "delete_event", G_CALLBACK(delete_cb), result);
  g_signal_connect(result->window, "scroll_event", G_CALLBACK(scroll_cb), result);

  g_object_ref(result->window);

  result->extra_filter_keypress = NULL;

  return result;
}

guint
candwin_get_index(CandWin* candwin) {
  return candwin->markup_index;
}

void
candwin_set_index(CandWin* candwin, guint index) {
  candwin->init_index = index;
  candwin->markup_index = index;
}

void
candwin_set_candlist(CandWin* candwin, GSList* cand_slist) {
  CandWin* tmpcand = g_new0(CandWin, 1);
  tmpcand->cand_slist = cand_slist;

  /* Clear all if cand_slist is newly set */
  tmpcand->window = candwin->window;
  memmove(candwin, tmpcand, sizeof(CandWin));
  g_free(tmpcand);

  /* Init index */
  candwin->init_index = 0;
  candwin->markup_index = 0;
}

void
candwin_set_extra_keyfunc(CandWin* candwin, CandWinKeyPressFunc func, gpointer data) {
  candwin->extra_filter_keypress = func;
  candwin->keyfunc_data = data;
}

