/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include <jllib.h>
#include <gtk/gtk.h>

#include <gdk/gdkkeysyms.h>

#include "freewnn-utils.h"
#include "freewnn-utils-ui.h"
#include "im-freewnn-intl.h"

#include "freewnn-register-win.h"

#define PHASE_MAX 4

typedef struct _Phase0 {
  GtkWidget* dic_combobox;
} Phase0;

typedef struct _Phase1 {
  GtkWidget* newword_entry;
  GtkWidget* phonetic_entry;
  GtkWidget* comment_entry;
  GtkWidget* freq_spinbutton;
  GtkWidget* class_combobox;
} Phase1;

typedef struct _Phase2 {
  w_char* class_node;
  gint class_combobox_length;
  GtkWidget* newword_label;
  GtkWidget* phonetic_label;
  GtkWidget* comment_label;
  GtkWidget* freq_label;
  GtkWidget* class_combobox;
} Phase2;

typedef struct _Phase3 {
  w_char* class_node;
  GtkWidget* newword_label;
  GtkWidget* phonetic_label;
  GtkWidget* comment_label;
  GtkWidget* freq_label;
  GtkWidget* class_label;
} Phase3;

typedef struct _NewWord {
  gchar* text;
  gchar* phonetic;
  gchar* comment;
  gint freq;
  w_char* class_node;
} NewWord;

struct _FreeWnnRegisterWin {
  GtkWidget* window;
  GtkWidget* vbox;
  struct wnn_buf* wnnbuf;
  GtkWidget* phase_container[PHASE_MAX];
  WNN_DIC_INFO* target_dic;
  Phase0* phase0;
  Phase1* phase1;
  Phase2* phase2;
  Phase3* phase3;
  NewWord* newword;
  GtkWidget* prev_button;
  GtkWidget* next_button;
  GtkWidget* finish_button;
  GtkWidget* cancel_button;
  gint phase;
  GSList* dic_slist;
};

static w_char* wchardup(w_char* wnnchar);
static void freewnn_register_win_show_next_button(FreeWnnRegisterWin* win);
static void freewnn_register_win_show_finish_button(FreeWnnRegisterWin* win);
static gboolean freewnn_register_win_word_add(FreeWnnRegisterWin* win);
static void phase1_clear_ui(Phase1* phase1);
static w_char* phase1_get_hinsi(Phase1* phase1, struct wnn_buf* buf);
static gboolean phase1_check_input(Phase1* phase1);
static void phase1_apply_newword(Phase1* phase1, NewWord* newword);
static void phase2_clear_combobox(Phase2* phase2);
static void phase2_set_class_node(Phase2* phase2, w_char* class_node);
static void phase2_append_hinsi_list(Phase2* phase2, struct wnn_buf* buf);
static w_char* phase2_get_hinsi(Phase2* phase2, struct wnn_buf* buf);
static void phase2_set_newword(Phase2* phase2, NewWord* newword);
static void phase2_apply_newword(Phase2* phase2, NewWord* newword);
static void phase3_set_newword(Phase3* phase3, NewWord* newword);
static void phase3_set_class_node(Phase3* phase3, w_char* class_node);
static void newword_clear(NewWord* newword);
static void newword_set_class_node(NewWord* newword, w_char* class_node);

static w_char*
wchardup(w_char* wnnchar) {
  gint i=0;
  gint max = 0;
  w_char* result = NULL;

  if( !wnnchar )
    return NULL;

  while(wnnchar[i++] != 0);

  max = i;
  result = g_new0(w_char, max);
  for(i=0;i<max;i++) {
    result[i] = wnnchar[i];
  }

  return result;
}

static void
freewnn_register_win_show_next_button(FreeWnnRegisterWin* win) {
  gtk_widget_hide(win->finish_button);
  gtk_widget_show(win->next_button);
  gtk_widget_grab_focus(win->next_button);
}

static void
freewnn_register_win_show_finish_button(FreeWnnRegisterWin* win) {
  gtk_widget_hide(win->next_button);
  gtk_widget_show(win->finish_button);
  gtk_widget_grab_focus(win->finish_button);
}

static gboolean
check_input_sub(GtkWidget* widget) {
  const gchar* text;
  g_return_val_if_fail(GTK_IS_ENTRY(widget), FALSE);
  text = gtk_entry_get_text(GTK_ENTRY(widget));

  return (*text == '\0');
}

static void
phase1_clear_ui(Phase1* phase1) {
  gtk_entry_set_text(GTK_ENTRY(phase1->newword_entry), "");
  gtk_entry_set_text(GTK_ENTRY(phase1->phonetic_entry), "");
  gtk_entry_set_text(GTK_ENTRY(phase1->comment_entry), "");
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(phase1->freq_spinbutton), 0.0);
  gtk_combo_box_set_active(GTK_COMBO_BOX(phase1->class_combobox), 0);
}

static gboolean
phase1_check_input(Phase1* phase1) {
  if( check_input_sub(phase1->newword_entry)) {
    freewnn_show_warning(_("New word entry is empty."));
    gtk_widget_grab_focus(phase1->newword_entry);
    return FALSE;
  }

  if( check_input_sub(phase1->phonetic_entry)) {
    freewnn_show_warning(_("Phonetic text is enpty."));
    gtk_widget_grab_focus(phase1->phonetic_entry);
    return FALSE;
  }

  return TRUE;
}

static w_char*
phase1_get_hinsi(Phase1* phase1, struct wnn_buf* buf) {
  gint i = 0;
  gint ret = 0;
  gint active = gtk_combo_box_get_active(GTK_COMBO_BOX(phase1->class_combobox));
  w_char** area = NULL;
  w_char* root_node = char2wchar("/");
  ret = jl_hinsi_list(buf, 10, root_node, &area);
  g_assert(ret > active);
  g_free(root_node);
  return wchardup(area[active]);
}

static void
phase1_apply_newword(Phase1* phase1, NewWord* newword) {
  gchar* text = NULL;

  newword_clear(newword);

  text = (gchar*)gtk_entry_get_text(GTK_ENTRY(phase1->newword_entry));
  newword->text = g_strdup(text);
  text = (gchar*)gtk_entry_get_text(GTK_ENTRY(phase1->phonetic_entry));
  newword->phonetic = g_strdup(text);
  text = (gchar*)gtk_entry_get_text(GTK_ENTRY(phase1->comment_entry));
  newword->comment = g_strdup(text);
  newword->freq = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(phase1->freq_spinbutton));
}

static void
phase2_clear_combobox(Phase2* phase2) {
  gint i = 0;
  if( phase2->class_combobox_length == 0 )
    return;

  for(i=0;i<phase2->class_combobox_length;i++) {
    gtk_combo_box_remove_text(GTK_COMBO_BOX(phase2->class_combobox), 0);
  } 
  gtk_combo_box_set_active(GTK_COMBO_BOX(phase2->class_combobox), -1);

  phase2->class_combobox_length = 0;
}

static void
phase2_set_class_node(Phase2* phase2, w_char* class_node) {
  if( phase2->class_node ) {
    g_free(phase2->class_node);
  }
  if( class_node )
    phase2->class_node = wchardup(class_node);
  else
    phase2->class_node = NULL;
}

static void
phase2_append_hinsi_list(Phase2* phase2, struct wnn_buf* buf) {
  gint i = 0;
  gint ret = 0;
  w_char** area = NULL;

  if( !phase2->class_node )
    return;

  ret = jl_hinsi_list(buf, 10, phase2->class_node, &area);
  for(i=0;i<ret;i++) {
    gchar* text = wchar_to_utf8(area[i]);
    gtk_combo_box_append_text(GTK_COMBO_BOX(phase2->class_combobox), text);
    g_free(text);
  }
  gtk_combo_box_set_active(GTK_COMBO_BOX(phase2->class_combobox), 0);
  phase2->class_combobox_length = ret;
}

static w_char*
phase2_get_hinsi(Phase2* phase2, struct wnn_buf* buf) {
  gint i = 0;
  gint ret = 0;
  gint active = gtk_combo_box_get_active(GTK_COMBO_BOX(phase2->class_combobox));
  w_char** area = NULL;
  ret = jl_hinsi_list(buf, 10, phase2->class_node, &area);
  g_assert(phase2->class_combobox_length == ret);
  g_assert(ret > active);
  return wchardup(area[active]);
}

static void
phase3_set_newword(Phase3* phase3, NewWord* newword) {
  gchar* freq_text = NULL;
  gchar* class_text = NULL;
  gtk_label_set_text(GTK_LABEL(phase3->newword_label), newword->text);
  gtk_label_set_text(GTK_LABEL(phase3->phonetic_label), newword->phonetic);
  gtk_label_set_text(GTK_LABEL(phase3->comment_label), newword->comment);
  freq_text = g_strdup_printf("%d", newword->freq);
  gtk_label_set_text(GTK_LABEL(phase3->freq_label), freq_text);
  g_free(freq_text);
  newword_set_class_node(newword, phase3->class_node);
  class_text = wchar_to_utf8(phase3->class_node);
  gtk_label_set_text(GTK_LABEL(phase3->class_label), class_text);
  g_free(class_text);
}

static void
phase2_set_newword(Phase2* phase2, NewWord* newword) {
  gchar* label_text = NULL;
  gtk_label_set_text(GTK_LABEL(phase2->newword_label), newword->text);
  gtk_label_set_text(GTK_LABEL(phase2->phonetic_label), newword->phonetic);
  gtk_label_set_text(GTK_LABEL(phase2->comment_label), newword->comment);
  label_text = g_strdup_printf("%d", newword->freq);
  gtk_label_set_text(GTK_LABEL(phase2->freq_label), label_text);
  g_free(label_text);
}

static void
phase3_set_class_node(Phase3* phase3, w_char* class_node) {
  if( phase3->class_node )
    g_free(phase3->class_node);

  if( class_node )
    phase3->class_node = wchardup(class_node);
  else
    phase3->class_node = NULL;
}

static void
newword_clear(NewWord* newword) {
  if( newword->text )
    g_free(newword->text);
  newword->text = NULL;

  if( newword->phonetic )
    g_free(newword->phonetic);
  newword->phonetic = NULL;

  if( newword->comment )
    g_free(newword->comment);
  newword->comment = NULL;

  newword->freq = 0;

  if( newword->class_node )
    g_free(newword->class_node);
  newword->class_node = NULL;
}

static void
newword_set_class_node(NewWord* newword, w_char* class_node) {
  if( newword->class_node )
    g_free(newword->class_node);
  newword->class_node = wchardup(class_node);
}

static gboolean
freewnn_register_win_word_add(FreeWnnRegisterWin* win) {
  int dic_no = win->target_dic->dic_no;
  w_char* yomi = utf8_to_wchar(win->newword->phonetic);
  w_char* kanji = utf8_to_wchar(win->newword->text);
  w_char* com = NULL;
  int hinsi = 0;
  int hindo = 0;
  int ret = 0;
  gboolean result = FALSE;

  if( win->newword->comment == NULL || *win->newword->comment != '\0' ) {
    com = NULL;
  } else {
    com = utf8_to_wchar(win->newword->comment);
  }

  hinsi = jl_hinsi_number(win->wnnbuf, win->newword->class_node);
  g_assert(hinsi != -1);
  hindo = win->newword->freq;

  ret = jl_word_add(win->wnnbuf, dic_no, yomi, kanji, com, hinsi, hindo);

  if( ret == -1 ) {
    freewnn_show_warning(_("Couldn't add the new word."));
    result = FALSE;
  } else {
    freewnn_show_message(_("Added the new word."));
    result = TRUE;
  }

  g_free(yomi);
  g_free(kanji);
  if( com )
    g_free(com);

  return result;
}

static GtkWidget* create_phase0_container(FreeWnnRegisterWin* win);
static GtkWidget* create_phase1_container(FreeWnnRegisterWin* win);
static GtkWidget* create_phase2_container(FreeWnnRegisterWin* win);
static GtkWidget* create_phase3_container(FreeWnnRegisterWin* win);

static void
show_phase(FreeWnnRegisterWin* win, gint newphase) {
  if( newphase == win->phase )
    return;

  if( newphase < 0 || newphase >= PHASE_MAX )
    return;

  gtk_container_remove(GTK_CONTAINER(win->vbox), win->phase_container[win->phase]);
  gtk_box_pack_start(GTK_BOX(win->vbox), win->phase_container[newphase], TRUE, TRUE, 0);
  gtk_box_reorder_child(GTK_BOX(win->vbox), win->phase_container[newphase], 0);
  gtk_widget_show_all(win->window);
  freewnn_register_win_show_next_button(win);

  if( newphase == 0 ) {
    gtk_widget_set_sensitive(win->prev_button, FALSE);
  } else {
    gtk_widget_set_sensitive(win->prev_button, TRUE);
  }

  if( newphase == 1 ) {
    phase2_clear_combobox(win->phase2);
    phase2_set_class_node(win->phase2, NULL);
    phase3_set_class_node(win->phase3, NULL);
  }

  if( newphase == 2 ) {
    phase3_set_class_node(win->phase3, NULL);
  }

  if( newphase == 3 ) {
    freewnn_register_win_show_finish_button(win);
  }

  win->phase = newphase;
}

static void
prev_button_cb(GtkWidget* widget, FreeWnnRegisterWin* win) {
  gint i = 0;

  switch(win->phase) {
  case 1:
    show_phase(win, 0);
    break;
  case 2:
    show_phase(win, 1);
    break;
  case 3:
    if( win->phase2->class_combobox_length == 0 ) {
      show_phase(win, 1);
    } else {
      show_phase(win, 2);
    }
    break;
  case 0:
  default:
    g_assert_not_reached();
    break;
  }
}

static void
next_button_cb(GtkWidget* widget, FreeWnnRegisterWin* win) {
  GtkTreeIter iter;
  gint ret = 0;
  gint active = 0;
  gint i = 0;
  w_char* tmp = NULL;
  w_char** area = NULL;
  w_char* new_class = NULL;
  w_char* phase_node = NULL;
  gboolean checked = FALSE;

  switch(win->phase) {
  case 0:
    win->target_dic = g_slist_nth_data(win->dic_slist, gtk_combo_box_get_active(GTK_COMBO_BOX(win->phase0->dic_combobox)));
    show_phase(win, 1);
    break;
  case 1:
    checked = phase1_check_input(win->phase1);
    if( !checked )
      break;
    phase1_apply_newword(win->phase1, win->newword);
    phase_node = phase1_get_hinsi(win->phase1, win->wnnbuf);
    ret = jl_hinsi_list(win->wnnbuf, 10, phase_node, &area);
    if( ret == 0 ) {
      newword_set_class_node(win->newword, phase_node);
      phase3_set_class_node(win->phase3, phase_node);
      g_free(phase_node);
      show_phase(win, 3);
      break;
    }
    phase2_set_class_node(win->phase2, phase_node);
    phase2_append_hinsi_list(win->phase2, win->wnnbuf);
    phase2_set_newword(win->phase2, win->newword);
    newword_set_class_node(win->newword, phase_node);
    g_free(phase_node);
    show_phase(win, 2);
    break;
  case 2:
    phase_node = phase2_get_hinsi(win->phase2, win->wnnbuf);
    phase3_set_class_node(win->phase3, phase_node);
    g_free(phase_node);
    phase3_set_newword(win->phase3, win->newword);
    show_phase(win, 3);
    break;
  case 3:
  default:
    g_assert_not_reached();
    break;
  }
}

static void
cancel_button_cb(GtkWidget* widget, FreeWnnRegisterWin* win) {
  freewnn_register_win_hide(win);
}

static void
finish_button_cb(GtkWidget* widget, FreeWnnRegisterWin* win) {
  gboolean result = freewnn_register_win_word_add(win);
  freewnn_register_win_hide(win);
}

static gboolean
keypress_cb(GtkWidget* widget, GdkEventKey* event, FreeWnnRegisterWin* win) {
  if( event->keyval == GDK_Escape ) {
     freewnn_register_win_hide(win); 
     return TRUE;
  }

  return FALSE;
}

GSList*
freewnn_get_writable_dic(struct wnn_buf* buf) {
  GSList* slist = NULL;
  WNN_DIC_INFO *dicinfo = NULL; 
  int dicnum = 0;
  gint i = 0;
  dicnum = jl_dic_list(buf, &dicinfo);

  if( dicnum == -1 || dicnum == 0 ) {
    return NULL;
  }

  for(i=0;i<dicnum;i++) {
    if( dicinfo[i].rw == WNN_DIC_RW
        && (dicinfo[i].type == WNN_UD_DICT
            || dicinfo[i].type == WNN_REV_DICT) ) {
      slist = g_slist_append(slist, &dicinfo[i]);
    }
  }

  return slist;
}

FreeWnnRegisterWin*
freewnn_register_win_new(struct wnn_buf* buf) {
  FreeWnnRegisterWin* win = g_new0(FreeWnnRegisterWin, 1);
  win->phase0 = g_new0(Phase0, 1);
  win->phase1 = g_new0(Phase1, 1);
  win->phase2 = g_new0(Phase2, 1);
  win->phase3 = g_new0(Phase3, 1);
  win->newword = g_new0(NewWord, 1);
  win->wnnbuf = buf;

  GtkWidget* hseparator = NULL;
  GtkWidget* hbuttonbox = NULL;

  win->dic_slist = freewnn_get_writable_dic(win->wnnbuf);
  if( !win->dic_slist ) {
    return NULL;
  }

  win->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(win->window), _("Add New Word"));
  g_signal_connect(win->window, "key_press_event", G_CALLBACK(keypress_cb), win);

  win->vbox = gtk_vbox_new(FALSE, 5);
  gtk_container_add(GTK_CONTAINER(win->window), win->vbox);
  gtk_container_set_border_width(GTK_CONTAINER(win->vbox), 5);

  hseparator = gtk_hseparator_new();
  gtk_box_pack_start(GTK_BOX(win->vbox), hseparator, TRUE, FALSE, 0);

  hbuttonbox = gtk_hbutton_box_new();
  gtk_box_pack_start(GTK_BOX(win->vbox), hbuttonbox, FALSE, FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(hbuttonbox), 5);
  gtk_button_box_set_layout(GTK_BUTTON_BOX(hbuttonbox), GTK_BUTTONBOX_END);
  gtk_box_set_spacing(GTK_BOX(hbuttonbox), 5);

  win->cancel_button = gtk_button_new_from_stock("gtk-cancel");
  g_signal_connect(win->cancel_button, "clicked", G_CALLBACK(cancel_button_cb), win);
  gtk_container_add(GTK_CONTAINER(hbuttonbox), win->cancel_button);

  win->prev_button = gtk_button_new_from_stock("gtk-go-back");
  g_signal_connect(win->prev_button, "clicked", G_CALLBACK(prev_button_cb), win);
  gtk_container_add(GTK_CONTAINER(hbuttonbox), win->prev_button);

  win->next_button = gtk_button_new_from_stock("gtk-go-forward");
  g_signal_connect(win->next_button, "clicked", G_CALLBACK(next_button_cb), win);

  gtk_container_add(GTK_CONTAINER(hbuttonbox), win->next_button);
  GTK_WIDGET_SET_FLAGS(win->next_button, GTK_CAN_DEFAULT);
  gtk_widget_grab_focus(win->next_button);

  win->finish_button = gtk_button_new_with_mnemonic(_("_Finish"));
  g_signal_connect(win->finish_button, "clicked", G_CALLBACK(finish_button_cb), win);
  gtk_container_add(GTK_CONTAINER(hbuttonbox), win->finish_button);

  win->phase_container[0] = create_phase0_container(win);
  win->phase_container[1] = create_phase1_container(win);
  win->phase_container[2] = create_phase2_container(win);
  win->phase_container[3] = create_phase3_container(win);

  gtk_widget_set_sensitive(win->prev_button, FALSE);

  return win;
}

/*
 *  freewnn_register_win_show does show FreeWnnRegisterWin window
 *  and initialize it
 */
void
freewnn_register_win_show(FreeWnnRegisterWin* win) {
  if( !win ) {
    freewnn_show_warning(_("No writable dictionary."));
    return;
  }

  if( GTK_WIDGET_VISIBLE(win->window) )
    return;

  /* Initialize */
  win->phase = 0;
  gtk_widget_set_sensitive(win->prev_button, FALSE);
  gtk_box_pack_start(GTK_BOX(win->vbox), win->phase_container[0], TRUE, TRUE, 0);
  gtk_box_reorder_child(GTK_BOX(win->vbox), win->phase_container[0], 0);
  newword_clear(win->newword);
  phase1_clear_ui(win->phase1);

  /* Show the widgets */
  gtk_widget_show_all(win->window);
  freewnn_register_win_show_next_button(win);
}

/*
 * freewnn_register_win_hide does hide.
 */
void
freewnn_register_win_hide(FreeWnnRegisterWin* win) {
  gtk_widget_hide_all(win->window);
  gtk_container_remove(GTK_CONTAINER(win->vbox), win->phase_container[win->phase]);
}

/* Destroy */
void
freewnn_register_win_finalize(FreeWnnRegisterWin* win) {
}

static GtkWidget*
create_phase0_container(FreeWnnRegisterWin* win) {
  GtkWidget* phase_table = NULL;
  GtkWidget* label = NULL;
  GtkWidget* dic_combobox = NULL;
  gint i = 0;

  phase_table = gtk_table_new(1, 2, FALSE);
  gtk_container_set_border_width(GTK_CONTAINER(phase_table), 5);
  gtk_table_set_row_spacings(GTK_TABLE(phase_table), 5);

  label = gtk_label_new(_("Target Dictionary: "));
  gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label, 0, 1, 0, 1,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  dic_combobox = gtk_combo_box_new_text();
  gtk_table_attach(GTK_TABLE(phase_table), dic_combobox, 1, 2, 0, 1,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (GTK_FILL), 0, 0);
  for(i=0;i<g_slist_length(win->dic_slist);i++) {
    WNN_DIC_INFO* info = g_slist_nth_data(win->dic_slist, i);
    gtk_combo_box_append_text(GTK_COMBO_BOX(dic_combobox), info->fname);
  }
  gtk_combo_box_set_active(GTK_COMBO_BOX(dic_combobox), 0);

  gtk_widget_show_all(phase_table); 
  g_object_ref(phase_table);

  win->phase0->dic_combobox = dic_combobox;

  return phase_table;
}

static GtkWidget*
create_phase1_container(FreeWnnRegisterWin* win) {
  GtkWidget* phase_table = NULL;
  GtkWidget* label1 = NULL;
  GtkWidget* label2 = NULL;
  GtkWidget* label3 = NULL;
  GtkWidget* label4 = NULL;
  GtkWidget* label5 = NULL;
  GtkWidget* newword_entry = NULL;
  GtkWidget* phonetic_entry = NULL;
  GtkWidget* comment_entry = NULL;
  GtkObject* spinbutton_adj = NULL;
  GtkWidget* freq_spinbutton = NULL;
  GtkWidget* class_combobox = NULL;
  gint ret = 0;
  w_char** area;
  gint i = 0;
  w_char* tmp = NULL;

  phase_table = gtk_table_new(5, 2, FALSE);
  gtk_container_set_border_width(GTK_CONTAINER(phase_table), 5);
  gtk_table_set_row_spacings(GTK_TABLE(phase_table), 5);

  label1 = gtk_label_new(_("New Word: "));
  gtk_misc_set_alignment(GTK_MISC(label1), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label1, 0, 1, 0, 1,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  newword_entry = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(phase_table), newword_entry, 1, 2, 0, 1,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label2 = gtk_label_new(_("Phono: "));
  gtk_misc_set_alignment(GTK_MISC(label2), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label2, 0, 1, 1, 2,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  phonetic_entry = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(phase_table), phonetic_entry, 1, 2, 1, 2,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label3 = gtk_label_new(_("Comment: "));
  gtk_misc_set_alignment(GTK_MISC(label3), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label3, 0, 1, 2, 3,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  comment_entry = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(phase_table), comment_entry, 1, 2, 2, 3,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label4 = gtk_label_new(_("Frequency: "));
  gtk_misc_set_alignment(GTK_MISC(label4), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label4, 0, 1, 3, 4,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  spinbutton_adj = gtk_adjustment_new(0, 0, 100, 1, 10, 10);
  freq_spinbutton = gtk_spin_button_new(GTK_ADJUSTMENT(spinbutton_adj), 1, 0);
  gtk_table_attach(GTK_TABLE(phase_table), freq_spinbutton, 1, 2, 3, 4,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label5 = gtk_label_new(_("Class: "));
  gtk_misc_set_alignment(GTK_MISC(label5), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label5, 0, 1, 4, 5,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  class_combobox = gtk_combo_box_new_text();
  gtk_table_attach(GTK_TABLE(phase_table), class_combobox, 1, 2, 4, 5,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (GTK_FILL), 0, 0);

  tmp = char2wchar("/");
  ret = jl_hinsi_list(win->wnnbuf, 10, tmp, &area);
  g_free(tmp);
  for(i=0;i<ret;i++) {
    gchar* text = wchar_to_utf8(area[i]);
    gtk_combo_box_append_text(GTK_COMBO_BOX(class_combobox), text);
    g_free(text);
  }
  gtk_combo_box_set_active(GTK_COMBO_BOX(class_combobox), 0);

  gtk_widget_show_all(phase_table);
  g_object_ref(phase_table);

  win->phase1->newword_entry = newword_entry;
  win->phase1->phonetic_entry = phonetic_entry;
  win->phase1->comment_entry = comment_entry;
  win->phase1->freq_spinbutton = freq_spinbutton;
  win->phase1->class_combobox = class_combobox;

  return phase_table;
}

static GtkWidget*
create_phase2_container(FreeWnnRegisterWin* win) {
  GtkWidget* phase_table = NULL;
  GtkWidget* label1 = NULL;
  GtkWidget* label2 = NULL;
  GtkWidget* label3 = NULL;
  GtkWidget* label4 = NULL;
  GtkWidget* label5 = NULL;
  GtkWidget* newword_label = NULL;
  GtkWidget* phonetic_label = NULL;
  GtkWidget* comment_label = NULL;
  GtkWidget* freq_label = NULL;
  GtkWidget* class_combobox = NULL;

  phase_table = gtk_table_new(5, 2, FALSE);
  gtk_container_set_border_width(GTK_CONTAINER(phase_table), 5);
  gtk_table_set_row_spacings(GTK_TABLE(phase_table), 5);

  label1 = gtk_label_new(_("New Word: "));
  gtk_misc_set_alignment(GTK_MISC(label1), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label1, 0, 1, 0, 1,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  newword_label = gtk_label_new("");
  gtk_table_attach(GTK_TABLE(phase_table), newword_label, 1, 2, 0, 1,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label2 = gtk_label_new(_("Phono: "));
  gtk_misc_set_alignment(GTK_MISC(label2), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label2, 0, 1, 1, 2,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  phonetic_label = gtk_label_new("");
  gtk_table_attach(GTK_TABLE(phase_table), phonetic_label, 1, 2, 1, 2,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label3 = gtk_label_new(_("Comment: "));
  gtk_misc_set_alignment(GTK_MISC(label3), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label3, 0, 1, 2, 3,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  comment_label = gtk_label_new("");
  gtk_table_attach(GTK_TABLE(phase_table), comment_label, 1, 2, 2, 3,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label4 = gtk_label_new(_("Frequency: "));
  gtk_misc_set_alignment(GTK_MISC(label4), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label4, 0, 1, 3, 4,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  freq_label = gtk_label_new("");
  gtk_table_attach(GTK_TABLE(phase_table), freq_label, 1, 2, 3, 4,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label5 = gtk_label_new(_("Class: "));
  gtk_misc_set_alignment(GTK_MISC(label5), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label5, 0, 1, 4, 5,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  class_combobox = gtk_combo_box_new_text();
  gtk_table_attach(GTK_TABLE(phase_table), class_combobox, 1, 2, 4, 5,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (GTK_FILL), 0, 0);

  gtk_widget_show_all(phase_table);
  g_object_ref(phase_table);

  win->phase2->newword_label = newword_label;
  win->phase2->phonetic_label = phonetic_label;
  win->phase2->comment_label = comment_label;
  win->phase2->freq_label = freq_label;
  win->phase2->class_combobox = class_combobox;

  return phase_table;
}

static GtkWidget*
create_phase3_container(FreeWnnRegisterWin* win) {
  GtkWidget* phase_table = NULL;
  GtkWidget* label1 = NULL;
  GtkWidget* label2 = NULL;
  GtkWidget* label3 = NULL;
  GtkWidget* label4 = NULL;
  GtkWidget* label5 = NULL;
  GtkWidget* newword_label = NULL;
  GtkWidget* phonetic_label = NULL;
  GtkWidget* comment_label = NULL;
  GtkWidget* freq_label = NULL;
  GtkWidget* class_label = NULL;

  phase_table = gtk_table_new(5, 2, FALSE);
  gtk_container_set_border_width(GTK_CONTAINER(phase_table), 5);
  gtk_table_set_row_spacings(GTK_TABLE(phase_table), 5);

  label1 = gtk_label_new(_("New Word: "));
  gtk_misc_set_alignment(GTK_MISC(label1), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label1, 0, 1, 0, 1,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  newword_label = gtk_label_new("");
  gtk_table_attach(GTK_TABLE(phase_table), newword_label, 1, 2, 0, 1,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label2 = gtk_label_new(_("Phono: "));
  gtk_misc_set_alignment(GTK_MISC(label2), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label2, 0, 1, 1, 2,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  phonetic_label = gtk_label_new("");
  gtk_table_attach(GTK_TABLE(phase_table), phonetic_label, 1, 2, 1, 2,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label3 = gtk_label_new(_("Comment: "));
  gtk_misc_set_alignment(GTK_MISC(label3), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label3, 0, 1, 2, 3,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  comment_label = gtk_label_new("");
  gtk_table_attach(GTK_TABLE(phase_table), comment_label, 1, 2, 2, 3,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label4 = gtk_label_new(_("Frequency: "));
  gtk_misc_set_alignment(GTK_MISC(label4), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label4, 0, 1, 3, 4,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  freq_label = gtk_label_new("");
  gtk_table_attach(GTK_TABLE(phase_table), freq_label, 1, 2, 3, 4,
                   (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                   (GtkAttachOptions) (0), 0, 0);

  label5 = gtk_label_new(_("Class: "));
  gtk_misc_set_alignment(GTK_MISC(label5), 1, 0.5);
  gtk_table_attach(GTK_TABLE(phase_table), label5, 0, 1, 4, 5,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (0), 1, 0.5);

  class_label = gtk_label_new("");
  gtk_table_attach(GTK_TABLE(phase_table), class_label, 1, 2, 4, 5,
                   (GtkAttachOptions) (GTK_FILL),
                   (GtkAttachOptions) (GTK_FILL), 0, 0);

  gtk_widget_show_all(phase_table);
  g_object_ref(phase_table);

  win->phase3->newword_label = newword_label;
  win->phase3->phonetic_label = phonetic_label;
  win->phase3->comment_label = comment_label;
  win->phase3->freq_label = freq_label;
  win->phase3->class_label = class_label;

  return phase_table;
}
