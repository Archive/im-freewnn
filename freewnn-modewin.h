/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#ifndef _FREEWNN_MODEWIN_H
#define _FREEWNN_MODEWIN_H

#include <gtk/gtk.h>

#include <jllib.h>

#include "freewnn-register-win.h"
#include "freewnn-config.h"

typedef struct _FreeWnnModeWin {
  GtkWidget* win0;
  GtkWidget* label0;

  GtkWidget* win1;
  GtkWidget* hbox1;
  GtkWidget* mode_button1; /* Button for mode change */
  GtkWidget* config_button1; /* Button for config dialog */
  GtkWidget* regist_button1; /* Button for regist dialog */

  /* Label text */
  gchar* label_text;

  /* ModeWin position */
  gint x;
  gint y;

  /* Signal handlers id */
  guint enter_hid;
  guint leave_hid;
  gboolean handlers_blocked;

  /* FreeWnn context */
  struct wnn_buf *buf;

  FreeWnnRegisterWin* register_win;
  FreeWnnConfig* config;

  FreeWnnFuncTable** functablep;

} FreeWnnModeWin;

FreeWnnModeWin* freewnn_modewin_new(struct wnn_buf* buf, FreeWnnFuncTable** functablep);
void freewnn_modewin_finalize(FreeWnnModeWin* modewin);
void freewnn_modewin_set_text(FreeWnnModeWin* modewin, gchar* text);
void freewnn_modewin_move(FreeWnnModeWin* modewin, gint x, gint y);
void freewnn_modewin_show(FreeWnnModeWin* modewin);
void freewnn_modewin_hide(FreeWnnModeWin* modewin);
void freewnn_modewin_size_request(FreeWnnModeWin* modewin, GtkRequisition* req);
void freewnn_modewin_finalize(FreeWnnModeWin* modewin);

#endif /* _FREEWNN_MODEWIN_H */
