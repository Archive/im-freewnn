#!/usr/bin/perl
#
# Perl script to display all ideographs for FreeWnn
#
# Copyright(C) 2004 Yukihiro Nakai <ynakai@redhat.com
#

open(EUC2UTF8, "| iconv -f EUC-JP -t UTF-8") || die "Can't open iconv.\n";
select(EUC2UTF8);

sub doprintrange {
  $r1 = $_[0];
  $r2 = $_[1];

  for($i=$r1;$i<=$r2;$i++) {
    doprint($i);
  }
}

sub doprint {
  $char = $_[0];
  $c1 = ($char & 0xFF00) >> 8;
  $c2 = $char & 0x00FF;
  print sprintf("%c%c", $c1 | 0x80, $c2 | 0x80);
}

doprintrange(0x2121,0x217e);
doprintrange (0x222e,0x217e);
doprintrange(0x223a,0x2141);
doprintrange(0x224a,0x2250);
doprintrange(0x225c,0x226a);
doprintrange(0x2272,0x2279);
doprint (0x227e);
doprint (0x246e);
doprint (0x2470);
doprint (0x2471);
doprint (0x256e);
doprint (0x2570);
doprint (0x2571);
doprint (0x2574);
doprint (0x2575);
doprint (0x2576);
doprintrange(0x2621,0x2638);
doprintrange(0x2641,0x2658);
doprintrange(0x2721,0x2741);
doprintrange(0x2751,0x2771);
doprintrange(0x2821,0x2840);
print "\n";

close(UTF2UTF8);
