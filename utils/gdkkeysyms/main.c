#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

static GHashTable* key_ht = NULL;

/* Make hash table for keycode -> key name */
GHashTable*
make_key_ht() {
  FILE* fp = NULL;
  gchar buf[256];
  GHashTable* result;

  fp = fopen("/usr/include/gtk-2.0/gdk/gdkkeysyms.h", "r");
  if( !fp ) {
    g_error("File not found\n");
    return NULL;
  }
  result = g_hash_table_new(NULL, NULL);
  while( fgets(buf, 256, fp) > 0 ) {
    gchar** tmpbufv = NULL;
    gchar* keystr = NULL, *keycodestr = NULL;
    guint keycode = 0;
    if( buf[strlen(buf)-1] != '\n' ) {
      g_error("File format is not correct\n");
      return NULL;
    }
    buf[strlen(buf)-1] = '\0';
    if( strncmp(buf, "#define GDK_", strlen("#define GDK_")) != 0 ) {
      continue;
    }
    tmpbufv = g_strsplit(buf, " ", -1);
    g_assert(!strcmp(tmpbufv[0], "#define"));
    g_assert(tmpbufv[1]);
    g_assert(tmpbufv[2]);

    if( strncmp(tmpbufv[2], "0x", 2) != 0 ) {
      continue;
    }
    
    keystr = tmpbufv[1];
    keycodestr = tmpbufv[2];
    keycode = strtoul(keycodestr, NULL, 16);
    if( g_hash_table_lookup(result, (gpointer)keycode) ) {
      gchar* str = g_hash_table_lookup(result, (gpointer)keycode);
      gchar* newstr = g_strconcat(str, ",", keystr, NULL);
      g_hash_table_insert(result, (gpointer)keycode, newstr);
      g_free(str);
    } else {
      g_hash_table_insert(result, (gpointer)keycode, g_strdup(keystr));
    }
    g_strfreev(tmpbufv);
  }
  fclose(fp);

  return result;
}

/* Convert from keycode to keyname */
gchar*
get_keyname(GHashTable* key_ht, guint keycode) {
  if( !key_ht )
    return NULL;

  return g_hash_table_lookup(key_ht, (gpointer)keycode);
}

void
keypress_cb(GtkWidget* widget, GdkEventKey* event) {
  if( event->keyval == GDK_Escape )
    gtk_main_quit();

  g_print("keypress: %s\n", get_keyname(key_ht, event->keyval));
}

int main(int argc, char** argv) {
  GtkWidget* window = NULL;

  gtk_set_locale();
  gtk_init(&argc, &argv);

  key_ht = make_key_ht();

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  g_signal_connect(window, "key_press_event", G_CALLBACK(keypress_cb), NULL);

  gtk_widget_show_all(window);

  gtk_main();

  return 0;
}
