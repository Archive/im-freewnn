/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#ifndef _KEYTABLE_H
#define _KEYTABLE_H

#include <gdk/gdkkeysyms.h>
#include <gdk/gdkevents.h>

typedef enum {
  FREEWNN_KEY_NONE = 0,
  FREEWNN_KEY_PREV_SEGMENT,
  FREEWNN_KEY_NEXT_SEGMENT,
  FREEWNN_KEY_FIRST_SEGNENT,
  FREEWNN_KEY_LAST_SEGMENT,
  FREEWNN_KEY_SEGMENT_LONGER,
  FREEWNN_KEY_SEGMENT_SHORTER,
  FREEWNN_KEY_SEGMENT_ALLLIST,
  //FREEWNN_KEY_???
  FREEWNN_KEY_DELETE_UNCOMMITTED,
  FREEWNN_KEY_TO_HIRAGANA,
  FREEWNN_KEY_TO_KATAKANA,
  FREEWNN_KEY_TO_HALF_KATAKANA,
  FREEWNN_KEY_HALF_ALNUM_MODE,
  FREEWNN_KEY_FULL_ALNUM_MODE,
  FREEWNN_KEY_KANJI_MODE,
  FREEWNN_KEY_INPUT_JISCODE,
  FREEWNN_KEY_CANCEL,
  FREEWNN_IDEOGRAPH_MODE,
} FreeWnnFuncKey;

#define MASK_CONTROL	GDK_CONTROL_MASK
#define MASK_SHIFT	GDK_SHIFT_MASK
#define MASK_ALT	GDK_MOD1_MASK
#define MASK_NONE	0

typedef struct _FreeWnnFuncTable {
  guint mask;
  guint gdk_keycode;
  FreeWnnFuncKey freewnn_func;
} FreeWnnFuncTable;

FreeWnnFuncKey key_to_freewnn_func(FreeWnnFuncTable* functable, GdkEventKey* key);

extern FreeWnnFuncTable kinput2style_functable[];
extern FreeWnnFuncTable eggstyle_functable[];

#endif /* _KEYTABLE_H */
