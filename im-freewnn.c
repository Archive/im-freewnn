/* im-freewnn - immodule for FreeWnn
 *
 * Copyright (C) 2000 Red Hat Software
 * Copyright (C) 2002 Yusuke Tabata
 * Copyright (C) 2003-2004 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Owen Taylor <otaylor@redhat.com>
 *          Yusuke Tabata <tee@kuis.kyoto-u.ac.jp>
 *          Yukihiro Nakai <ynakai@redhat.com>
 *
 */

#include <stdio.h>

#include <string.h>
#include "keytable.h"

#include <gdk/gdkkeysyms.h>

#include "im-freewnn-intl.h"
#include "convtable.h"
#include "im-freewnn.h"

#define USE_SNOOPER

#ifndef WNNENVRC
#define WNNENVRC "/etc/FreeWnn/ja/wnnenvrc"
#endif

typedef struct _keypair {
  guint keyval;
  gchar* utf8;
} keypair;

keypair full_width_table[] = {
  { GDK_colon, "：" },
  { GDK_semicolon, "；" },
  { GDK_backslash, "＼" },
  { GDK_slash, "／" },
  { GDK_comma, "、" },
  { GDK_period, "。" },
  { GDK_at, "＠" },
  { GDK_minus, "ー" },
  { 0, NULL },
};

static const gchar* ideos = "　、。，．・：；？！゛゜´｀¨＾￣＿ヽヾゝゞ〃仝々〆〇ー―‐／＼〜‖｜…‥‘’“”（）〔〕［］｛｝〈〉《》「」『』【】＋−±×÷＝≠＜＞≦≧∞∴♂♀°′″℃￥＄¢£％＃＆＊＠§☆★○●◎◇∧∨¬⇒⇔∀∃∠⊥⌒∂∇≡≒≪≫√∽∝∵∫∬Å‰♯♭♪†‡¶◯ゎゐゑヮヰヱヴヵヶΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρστυφχψωАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя─│┌┐┘└├┬┤┴┼━┃┏┓┛┗┣┳┫┻╋┠┯┨┷┿┝┰┥┸╂";

#ifdef BUFSIZ
#undef BUFSIZ
#define BUFSIZ 1024
#endif

GType type_freewnn = 0;

int debug_loop = 0;

#ifdef USE_SNOOPER
static guint snooper_id = 0;
#endif
static GtkIMContext* focused_context = NULL;

static void im_freewnn_class_init (GtkIMContextClass *class);
static void im_freewnn_init (GtkIMContext *im_context);
static void im_freewnn_finalize(GObject *obj);
static gboolean im_freewnn_filter_keypress(GtkIMContext *context,
		                GdkEventKey *key);
static void im_freewnn_get_preedit_string(GtkIMContext *, gchar **str,
					PangoAttrList **attrs,
					gint *cursor_pos);
static void im_freewnn_force_init(IMContextFreeWnn* cn);
static void im_freewnn_focus_in(GtkIMContext* context);
static void im_freewnn_focus_out(GtkIMContext* context);
static void im_freewnn_set_client_window(GtkIMContext* context, GdkWindow *win);
static gboolean im_freewnn_is_modechangekey(GtkIMContext *context, GdkEventKey *key);
static gboolean return_false(GtkIMContext* context, GdkEventKey* key);
static void freewnn_conversion(IMContextFreeWnn* cf);

static gchar* get_full_width(guint keyval);
static gint get_prefix_utf8_len(IMContextFreeWnn* cf, gint cseg);
static void goto_segment(IMContextFreeWnn* cf, gint segment);
static void update_modewin(IMContextFreeWnn* cf);
static void candwin_hide_cb(GtkWidget* widget, IMContextFreeWnn* cf);
static void candwin_ideo_hide_cb(GtkWidget* widget, IMContextFreeWnn* cf);
static void im_freewnn_commit(IMContextFreeWnn* cf);

/* UTF-8 version of FreeWnn funcs */
static int jl_utf8_get_kanji(struct wnn_buf* wnnbuf, int bun_no, int bun_no2, gchar* area);

static void
printError(char* s) {
  g_print("Error: %s\n", s);
}

static int
printConfirm(char* s) {
  g_print("Confirm: %s\n", s);
  return 1;
}

static int
strrncmp(char* s1, char* s2, size_t n) {
  char* s1tmp = s1;
  if( strlen(s1) >= n ) {
    s1tmp += strlen(s1)-n;
  }

  return strcmp(s1tmp, s2);
}

static char*
roma2kana(char* romastr) {
  char* result = NULL;
  int tsize = sizeof(kanatable)/sizeof(kanatable[0]);
  int i;

  if( romastr == NULL ) return NULL;
  if( *romastr == '\0' ) return NULL;

  for(i=1;i<=tsize;i++) {
    romapair rp = kanatable[tsize-i];
    if( strrncmp(romastr, rp.roma, strlen(rp.roma)) == 0 ) {
      int len = strlen(romastr)-strlen(rp.roma)+strlen(rp.kana)+1;
      result = (char*)calloc(len+1, sizeof(char));
      strncat(result, romastr, strlen(romastr)-strlen(rp.roma));
      strcat(result, rp.kana);
      return result;
    }
  }

  result = strdup(romastr); /* No change */

  return result;
}

static void
buffer_append(char* buf, char ch) {
  char* kana = NULL;

  if( buf == NULL ) return;

  buf[strlen(buf)+1] = '\0';
  buf[strlen(buf)] = ch;

  kana = roma2kana(buf);
  strcpy(buf, kana);

  if( kana )
    free(kana);
}

#ifdef USE_SNOOPER
static gboolean
snooper_func(GtkWidget* widget, GdkEventKey* event, gpointer data) {
  if( focused_context )
    return im_freewnn_filter_keypress(focused_context, event);

  return FALSE;
}
#endif

static gboolean
roma2kana_freewnn(GtkIMContext* context, gchar newinput) {
  return FALSE;
}

void
im_freewnn_register_type (GTypeModule *module)
{
  static const GTypeInfo object_info =
  {
    sizeof (IMContextFreeWnnClass),
    (GBaseInitFunc) NULL,
    (GBaseFinalizeFunc) NULL,
    (GClassInitFunc) im_freewnn_class_init,
    NULL,           /* class_finalize */
    NULL,           /* class_data */
    sizeof (IMContextFreeWnn),
    0,
    (GtkObjectInitFunc) im_freewnn_init,
  };

  type_freewnn = 
    g_type_module_register_type (module,
				 GTK_TYPE_IM_CONTEXT,
				 "IMContextFreeWnn",
				 &object_info, 0);
}

static void
im_freewnn_class_init (GtkIMContextClass *class)
{
  GtkIMContextClass *im_context_class = GTK_IM_CONTEXT_CLASS(class);
  GObjectClass *object_class = G_OBJECT_CLASS(class);

  im_context_class->set_client_window = im_freewnn_set_client_window;
#ifdef USE_SNOOPER
  im_context_class->filter_keypress = return_false;
#else
  im_context_class->filter_keypress = im_freewnn_filter_keypress;
#endif
  im_context_class->get_preedit_string = im_freewnn_get_preedit_string;
  im_context_class->focus_in = im_freewnn_focus_in;
  im_context_class->focus_out = im_freewnn_focus_out;

  object_class->finalize = im_freewnn_finalize;
}

static void
im_freewnn_init (GtkIMContext *im_context)
{
  IMContextFreeWnn *cf = IM_CONTEXT_FREEWNN(im_context);
  struct wnn_env* wenv = NULL;

#ifdef USE_SNOOPER
  snooper_id = gtk_key_snooper_install((GtkKeySnoopFunc)snooper_func, NULL);
#endif

  cf->wnnbuf = jl_open_lang("im-freewnn", getenv("JSERVER"), "ja_JP", NULL, printError, printConfirm, 0);

  if( cf->wnnbuf == NULL ) {
    g_print("jl_open_lang() failed.\n");
    freewnn_show_warning(_("Cannot connect to jserver: jl_open_lang() failed."));
    return;
  }

  wenv = jl_env_get(cf->wnnbuf);

  if( wenv == NULL ) {
    freewnn_show_warning(_("Cannot connect to jserver: jl_env_get() failed."));
    jl_close(cf->wnnbuf);
    cf->wnnbuf = NULL;
    return;
  }

  jl_set_env_wnnrc(wenv, WNNENVRC, printConfirm, printError);

  cf->modewin = freewnn_modewin_new(cf->wnnbuf, &cf->functable);
  cf->candwin = candwin_new();

  cf->preedit_buf = g_new0(gchar, BUFSIZ);
  cf->ja_mode = FALSE;
  cf->kanji_on = FALSE;
  cf->kanji_len = 0;
  cf->prefix_utf8_len = 0;
  cf->kanji_utf8_len = 0;

  cf->functable = kinput2style_functable;
}

static void
im_freewnn_finalize(GObject *obj) {
  IMContextFreeWnn* cf = IM_CONTEXT_FREEWNN(obj);

  if( !cf->wnnbuf )
    return;

#ifdef USE_SNOOPER
  if( snooper_id != 0 ) {
    gtk_key_snooper_remove(snooper_id);
    snooper_id = 0;
  }
#endif

  jl_close(cf->wnnbuf);
  g_free(cf->preedit_buf);

  freewnn_modewin_finalize(cf->modewin);
}

static const GtkIMContextInfo im_freewnn_info = { 
  "FreeWnn",		   /* ID */
  "FreeWnn",             /* Human readable name */
  "im-freewnn",			   /* Translation domain */
   IM_LOCALEDIR,		   /* Dir for bindtextdomain (not strictly needed for "gtk+") */
   ""			           /* Languages for which this module is the default */
};

static const GtkIMContextInfo *info_list[] = {
  &im_freewnn_info
};

void
im_module_init (GTypeModule *module)
{
  im_freewnn_register_type(module);
}

void 
im_module_exit (void)
{
}

void 
im_module_list (const GtkIMContextInfo ***contexts,
		int                      *n_contexts)
{
  *contexts = info_list;
  *n_contexts = G_N_ELEMENTS (info_list);
}

GtkIMContext *
im_module_create (const gchar *context_id)
{
  if (strcmp (context_id, "FreeWnn") == 0)
    return GTK_IM_CONTEXT (g_object_new (type_freewnn, NULL));
  else
    return NULL;
}

static gboolean
im_freewnn_filter_keypress(GtkIMContext *context, GdkEventKey *key)
{
  IMContextFreeWnn *cf = IM_CONTEXT_FREEWNN(context);
  gchar* utf8 = NULL;

  if( key->type == GDK_KEY_RELEASE ) {
    return FALSE;
  };

  //g_print("IM-FREEWNN: keyval=0x%04X\n", key->keyval);

  if( cf->wnnbuf && im_freewnn_is_modechangekey(context, key) ) {
    cf->ja_mode = cf->ja_mode ? FALSE : TRUE;
    update_modewin(cf);
    if( !cf->ja_mode && *cf->preedit_buf != '\0' ) {
      im_freewnn_commit(cf);
    }
    return TRUE;
  }

  /* English mode */
  if( !cf->wnnbuf || cf->ja_mode == FALSE ) {
    gunichar keyinput = gdk_keyval_to_unicode(key->keyval);
    gchar ubuf[7];
    gint len;

    /* Gdk handles unknown key as 0x0000 */
    if( keyinput == 0 )
      return FALSE;

    if( key->state & GDK_CONTROL_MASK )
      return FALSE;

    /* For real char keys */
    memset(ubuf, 0, 7);
    len = g_unichar_to_utf8(keyinput, ubuf);
    ubuf[len] = '\0';
    g_signal_emit_by_name(cf, "commit", ubuf);
    return TRUE;
  }

  /*** Japanese mode ***/
  /* No input char yet */
  if( *cf->preedit_buf == '\0' ) {
    if( key->state & GDK_CONTROL_MASK ) {
      switch(key->keyval) {
      case GDK_a:
      case GDK_b:
      case GDK_f:
      case GDK_e:
      case GDK_n:
      case GDK_p:
      case GDK_o:
        return FALSE;
        break;
      default:
        break;
      }
    }

    switch(key->keyval) {
    case GDK_space:
      g_signal_emit_by_name(cf, "commit", " ");
      return TRUE;
      break;
    case GDK_Return:
    case GDK_BackSpace:
    case GDK_Left:
    case GDK_Up:
    case GDK_Right:
    case GDK_Down:
    case GDK_Page_Up:
    case GDK_Page_Down:
    case GDK_End:
    case GDK_Insert:
    case GDK_Delete:
    case GDK_Shift_L:
    case GDK_Shift_R:
    case GDK_Home:
      return FALSE; /* Functions key handling depends on afterward codes */
      break;
    default:
      break;
    }
  }

  /* Input ideographs */
  utf8 = get_full_width(key->keyval);
  if( utf8 ) {
    if( *cf->preedit_buf == '\0' ) {
      g_signal_emit_by_name(cf, "commit", utf8);
    } else {
      strcat(cf->preedit_buf, utf8);
      g_signal_emit_by_name(cf, "preedit_changed");
    }
    return TRUE;
  }

  if( key->keyval == GDK_space && *cf->preedit_buf != '\0' ) {
    freewnn_conversion(cf);
    g_signal_emit_by_name(cf, "preedit_changed");
    update_modewin(cf);
    return TRUE;
  }

  /* Commit string with GDK_Return */
  if( key->keyval == GDK_Return && *cf->preedit_buf != '\0' ) {
    if( strlen(cf->preedit_buf) == 0 )
      return FALSE;

    im_freewnn_commit(cf);
    cf->kanji_on = FALSE;
    cf->kanji_utf8_len = 0;
    update_modewin(cf);
    return TRUE;
  }

  /* Envoke FreeWnn functions for each keys */
  if( cf->ja_mode ) {
    switch(key_to_freewnn_func(cf->functable, key)) {
    case FREEWNN_KEY_PREV_SEGMENT:
      goto_segment(cf, jl_zenkouho_bun(cf->wnnbuf)-1);
      return TRUE;
      break;
    case FREEWNN_KEY_NEXT_SEGMENT:
      goto_segment(cf, jl_zenkouho_bun(cf->wnnbuf)+1);
      return TRUE;
      break;
    case FREEWNN_KEY_FIRST_SEGNENT:
      goto_segment(cf, 0);
      return TRUE;
      break;
    case FREEWNN_KEY_LAST_SEGMENT:
      goto_segment(cf, jl_bun_suu(cf->wnnbuf)-1);
      return TRUE;
      break;
    case FREEWNN_KEY_SEGMENT_LONGER:
    {
      gchar kanjibuf[BUFSIZ];
      w_char yomibuf[BUFSIZ];
      gint current_seg = jl_zenkouho_bun(cf->wnnbuf);
      gint yomi_len = jl_get_yomi(cf->wnnbuf, current_seg, current_seg+1, yomibuf);
      if( yomi_len >= jl_get_yomi(cf->wnnbuf, current_seg, -1, yomibuf) ) {
        return TRUE;
      }
      jl_nobi_conv(cf->wnnbuf, current_seg, yomi_len+1, -1, WNN_NO_USE, WNN_SHO);
      jl_zenkouho(cf->wnnbuf, current_seg, WNN_NO_USE, WNN_UNIQ_KNJ);
      memset(kanjibuf, 0, sizeof(gchar)*BUFSIZ);
      cf->kanji_len = jl_utf8_get_kanji(cf->wnnbuf, 0, -1, kanjibuf);
      strcpy(cf->preedit_buf, kanjibuf);
      cf->prefix_utf8_len = get_prefix_utf8_len(cf, current_seg);
      jl_utf8_get_kanji(cf->wnnbuf, current_seg, current_seg+1, kanjibuf);
      cf->kanji_utf8_len = strlen(kanjibuf);

      g_signal_emit_by_name(cf, "preedit_changed");
    }
      return TRUE;
      break;
    case FREEWNN_KEY_SEGMENT_SHORTER:
    {
      gchar kanjibuf[BUFSIZ];
      w_char yomibuf[BUFSIZ];
      gint current_seg = jl_zenkouho_bun(cf->wnnbuf);
      gint yomi_len = jl_get_yomi(cf->wnnbuf, current_seg, current_seg+1, yomibuf);
      if( yomi_len <= 1 ) {
        return TRUE;
      }
      jl_nobi_conv(cf->wnnbuf, current_seg, yomi_len-1, -1, WNN_NO_USE, WNN_DAI);
      jl_zenkouho(cf->wnnbuf, current_seg, WNN_NO_USE, WNN_UNIQ_KNJ);
      memset(kanjibuf, 0, sizeof(gchar)*BUFSIZ);
      cf->kanji_len = jl_utf8_get_kanji(cf->wnnbuf, 0, -1, kanjibuf);
      strcpy(cf->preedit_buf, kanjibuf);
      cf->prefix_utf8_len = get_prefix_utf8_len(cf, current_seg);
      jl_utf8_get_kanji(cf->wnnbuf, current_seg, current_seg+1, kanjibuf);
      cf->kanji_utf8_len = strlen(kanjibuf);

      g_signal_emit_by_name(cf, "preedit_changed");
    }
      return TRUE;
      break;
    case FREEWNN_KEY_SEGMENT_ALLLIST:
      return TRUE;
      break;
    case FREEWNN_KEY_DELETE_UNCOMMITTED:
      return TRUE;
      break;
    case FREEWNN_KEY_TO_HIRAGANA:
      if( cf->kanji_on ) {
        gchar* utf8 = NULL;
        w_char yomibuf[BUFSIZ];
        gchar kanjibuf[BUFSIZ];
        gint yomi_len = jl_get_yomi(cf->wnnbuf, 0, -1, yomibuf);

        utf8 = wchar_to_utf8(yomibuf);
        strcpy(cf->preedit_buf, utf8);
        g_free(utf8);

        cf->prefix_utf8_len = 0;
        cf->kanji_utf8_len = 0;
        cf->kanji_on = FALSE;
        g_signal_emit_by_name(cf, "preedit_changed");
      } else {
        if( cf->preedit_buf != NULL && *cf->preedit_buf != '\0' ) {
          gchar* hiragana = im_anykana2hira(cf->preedit_buf);
          strcpy(cf->preedit_buf, hiragana);
          g_free(hiragana);
          g_signal_emit_by_name(cf, "preedit_changed");
        }
      }
      return TRUE;
      break;
    case FREEWNN_KEY_TO_KATAKANA:
      if( cf->kanji_on ) {
        gchar* utf8 = NULL;
        gchar* katakana = NULL;
        w_char yomibuf[BUFSIZ];
        gint yomi_len = jl_get_yomi(cf->wnnbuf, 0, -1, yomibuf);
        utf8 = wchar_to_utf8(yomibuf);
        katakana = im_anykana2kata(utf8);
        g_free(utf8);
        strcpy(cf->preedit_buf, katakana);
        g_free(katakana);
        cf->kanji_utf8_len = 0;
        cf->prefix_utf8_len = 0;
        cf->kanji_on = FALSE;
        g_signal_emit_by_name(cf, "preedit_changed");
      } else {
        if( cf->preedit_buf != NULL && *cf->preedit_buf != '\0' ) {
          gchar* katakana = im_anykana2kata(cf->preedit_buf);
          strcpy(cf->preedit_buf, katakana);
          g_free(katakana);
          g_signal_emit_by_name(cf, "preedit_changed");
        }
      }
      return TRUE;
      break;
    case FREEWNN_KEY_TO_HALF_KATAKANA:
      if( cf->kanji_on ) {
        gchar* utf8 = NULL;
        gchar* hkata = NULL;
        w_char yomibuf[BUFSIZ];
        gint yomi_len = jl_get_yomi(cf->wnnbuf, 0, -1, yomibuf);
        utf8 = wchar_to_utf8(yomibuf);
        hkata = im_anykana2hkata(utf8);
        g_free(utf8);
        strcpy(cf->preedit_buf, hkata);
        g_free(hkata);
        cf->kanji_utf8_len = 0;
        cf->prefix_utf8_len = 0;
        cf->kanji_on = FALSE;
        g_signal_emit_by_name(cf, "preedit_changed");
      } else {
        if( cf->preedit_buf != NULL && *cf->preedit_buf != '\0' ) {
          gchar* hkata = im_anykana2hkata(cf->preedit_buf);
          strcpy(cf->preedit_buf, hkata);
          g_free(hkata);
          g_signal_emit_by_name(cf, "preedit_changed");
        }
      }
      return TRUE;
      break;
    case FREEWNN_KEY_HALF_ALNUM_MODE:
      return TRUE;
      break;
    case FREEWNN_KEY_FULL_ALNUM_MODE:
      return TRUE;
      break;
    case FREEWNN_KEY_KANJI_MODE:
      return TRUE;
      break;
    case FREEWNN_KEY_INPUT_JISCODE:
      return TRUE;
      break;
    case FREEWNN_KEY_CANCEL:
      if( cf->kanji_on ) {
        w_char yomibuf[BUFSIZ];
        gchar* utf8 = NULL;
        jl_get_yomi(cf->wnnbuf, 0, -1, yomibuf);
        utf8 = wchar_to_utf8(yomibuf);
        strcpy(cf->preedit_buf, utf8);
        cf->prefix_utf8_len = 0;
        cf->kanji_utf8_len = 0;
        cf->kanji_on = FALSE;
        g_signal_emit_by_name(cf, "preedit_changed");
      }
      return TRUE;
      break;
    case FREEWNN_IDEOGRAPH_MODE:
    {
      gint i;
      GSList* ideo_slist = NULL;
      for(i=0;i<g_utf8_strlen(ideos, -1);i++) {
        gchar* cur = g_utf8_offset_to_pointer(ideos, i);
        gchar* cur2 = g_utf8_offset_to_pointer(cur, 1);
        ideo_slist = g_slist_append(ideo_slist, g_strndup(cur, cur2-cur));
      }
      cf->candwin = candwin_new();
      candwin_set_candlist(cf->candwin, ideo_slist);
      g_signal_connect(cf->candwin->window, "hide", G_CALLBACK(candwin_ideo_hide_cb), cf);
      gtk_widget_show_all(cf->candwin->window);
    }
      return TRUE;
      break;
    default:
      break;
    }
  }

  if(key->keyval >= GDK_a && key->keyval <= GDK_z) {
    if( cf->kanji_on ) {
      im_freewnn_commit(cf);
    }
    buffer_append(cf->preedit_buf, key->keyval);
    g_signal_emit_by_name(cf, "preedit_changed");
    return TRUE;
  }

  /* List up all keys which you want input like kana. */
  switch(key->keyval) {
  case GDK_1:
  case GDK_2:
  case GDK_3:
  case GDK_4:
  case GDK_5:
  case GDK_6:
  case GDK_7:
  case GDK_8:
  case GDK_9:
  case GDK_0:
  case GDK_asciicircum:
  case GDK_colon:
  case GDK_semicolon:
  case GDK_at:
  case GDK_minus:
  case GDK_backslash:
  case GDK_slash:
  case GDK_period:
  case GDK_comma:
  case GDK_bracketright:
  case GDK_bracketleft:
  case GDK_less:
  case GDK_greater:
  case GDK_question:
  case GDK_underscore:
  case GDK_plus:
  case GDK_asterisk:
  case GDK_quoteleft: /* = GDK_grave */
  case GDK_braceright:
  case GDK_braceleft:
  case GDK_exclam:
  case GDK_quotedbl:
  case GDK_numbersign:
  case GDK_dollar:
  case GDK_percent:
  case GDK_ampersand:
  case GDK_apostrophe: /* = GDK_quoteright */
  case GDK_parenleft:
  case GDK_parenright:
  case GDK_asciitilde:
  case GDK_equal:
  case GDK_bar:
/*
  case GDK_KP_0:
  case GDK_KP_1:
  case GDK_KP_2:
  case GDK_KP_3:
  case GDK_KP_4:
  case GDK_KP_5:
  case GDK_KP_6:
  case GDK_KP_7:
  case GDK_KP_8:
  case GDK_KP_9:
  case GDK_KP_Decimal:
  case GDK_KP_Divide:
  case GDK_KP_Multiply:
  case GDK_KP_Subtract:
  case GDK_KP_Add:
*/
    if( cf->kanji_on ) {
      im_freewnn_commit(cf);
    }
    buffer_append(cf->preedit_buf, key->keyval);
    g_signal_emit_by_name(cf, "preedit_changed");
    return TRUE;
    break;
  default:
    break;
  }

  if( key->keyval == GDK_BackSpace && *cf->preedit_buf != '\0' ) {
    if( cf->kanji_on == FALSE ) {
      gchar* last = g_utf8_offset_to_pointer(cf->preedit_buf, g_utf8_strlen(cf->preedit_buf, -1)-1);
      *last = '\0';
      g_signal_emit_by_name(cf, "preedit_changed");
      cf->prefix_utf8_len = 0;
      cf->kanji_utf8_len = 0;
    } else {
      w_char yomibuf[BUFSIZ];
      gchar* utf8 = NULL;
      jl_get_yomi(cf->wnnbuf, 0, -1, yomibuf);
      utf8 = wchar_to_utf8(yomibuf);
      strcpy(cf->preedit_buf, utf8);
      cf->prefix_utf8_len = 0;
      cf->kanji_utf8_len = 0;
      cf->kanji_on = FALSE;
      g_signal_emit_by_name(cf, "preedit_changed");
    }
    return TRUE;
  }

  if( *cf->preedit_buf != '\0' ) {
    g_warning("preedit area is floating on the app");
  }

  return FALSE;
}

void
im_freewnn_get_preedit_string(GtkIMContext *ic, gchar **str,
			    PangoAttrList **attrs, gint *cursor_pos)
{
  IMContextFreeWnn *cf = IM_CONTEXT_FREEWNN(ic);

  if ( !cf->wnnbuf ) {
    *str = g_strdup("");
    return;
  }

  *str = g_strdup(cf->preedit_buf);

  if (attrs) {
    *attrs = pango_attr_list_new();
  }
  if (cursor_pos) {
    *cursor_pos = 0;
  }

  if( *str == NULL || strlen(*str) == 0 ) return;

  if (attrs) {
    PangoAttribute *attr = NULL;
    attr = pango_attr_underline_new(PANGO_UNDERLINE_SINGLE);
    attr->start_index = 0;
    attr->end_index = strlen(*str);
    pango_attr_list_insert(*attrs, attr);

    attr = pango_attr_background_new(0, 0, 0);
    attr->start_index = cf->prefix_utf8_len;
    attr->end_index = cf->prefix_utf8_len + cf->kanji_utf8_len;
    pango_attr_list_insert(*attrs, attr);

    attr = pango_attr_foreground_new(0xffff, 0xffff, 0xffff);
    attr->start_index = cf->prefix_utf8_len;
    attr->end_index = cf->prefix_utf8_len + cf->kanji_utf8_len;
    pango_attr_list_insert(*attrs, attr);
  }

  if( cursor_pos )
    *cursor_pos = strlen(*str);
}

static void
im_freewnn_force_init(IMContextFreeWnn* cf) {
}

static void
im_freewnn_set_client_window(GtkIMContext* context, GdkWindow *win) {
  IMContextFreeWnn* cf = IM_CONTEXT_FREEWNN(context);

  if( !cf->wnnbuf )
    return;

  cf->client_window = win;
}

/* Mode change key combination (Shift+Space etc) or not? */
static gboolean
im_freewnn_is_modechangekey(GtkIMContext *context, GdkEventKey *key) {
  /* Kinput2 style - Shift + Space */
  if( key->state & GDK_SHIFT_MASK && key->keyval == GDK_space ) {
    return TRUE;
  /* Chinese/Korean style - Control + Space */
  } else if( key->state & GDK_CONTROL_MASK && key->keyval == GDK_space ) {
    return TRUE;
  /* Windows Style - Alt + Kanji */
  } else if( key->keyval == GDK_Kanji ) {
    return TRUE;
  /* Egg Style - Control + '\' */
  } else if ( key->state & GDK_CONTROL_MASK && key->keyval == GDK_backslash ) {
    return TRUE;
  }
  /* or should be customizable with dialog */

  return FALSE;
}

static void
im_freewnn_focus_in (GtkIMContext* context) {
  IMContextFreeWnn* cf = IM_CONTEXT_FREEWNN(context);

  focused_context = context;
}

static void
im_freewnn_focus_out (GtkIMContext* context) {
  IMContextFreeWnn* cf = IM_CONTEXT_FREEWNN(context);

  focused_context = NULL;
}

static gboolean
return_false(GtkIMContext* context, GdkEventKey* key) {
  return FALSE;
}

static void
candwin_ideo_hide_cb(GtkWidget* widget, IMContextFreeWnn* cf) {
  guint index = candwin_get_index(cf->candwin);

  const gchar* cur = g_utf8_offset_to_pointer(ideos, index);
  const gchar* cur2 = g_utf8_offset_to_pointer(cur, 1);
  gchar* buf = g_strndup(cur, cur2-cur);

  strcat(cf->preedit_buf, buf);
  g_free(buf);
                                                                                
  g_signal_emit_by_name(cf, "preedit_changed");
  update_modewin(cf);

  /* Destroy candwin HERE */
}

static void
candwin_hide_cb(GtkWidget* widget, IMContextFreeWnn* cf) {
  gchar kanjibuf[BUFSIZ];
  gint current_seg = 0;

  jl_set_jikouho(cf->wnnbuf, candwin_get_index(cf->candwin));

  current_seg = jl_zenkouho_bun(cf->wnnbuf);

  memset(kanjibuf, 0, sizeof(gchar)*BUFSIZ);
  cf->kanji_len = jl_utf8_get_kanji(cf->wnnbuf, 0, -1, kanjibuf);
  memset(cf->preedit_buf, 0, sizeof(gchar)*BUFSIZ);
  strcpy(cf->preedit_buf, kanjibuf);

  cf->prefix_utf8_len = get_prefix_utf8_len(cf, current_seg);
  jl_utf8_get_kanji(cf->wnnbuf, current_seg, current_seg+1, kanjibuf);
  cf->kanji_utf8_len = strlen(kanjibuf);

  g_signal_emit_by_name(cf, "preedit_changed");
  update_modewin(cf);

  /* Destroy candwin HERE */
}

static void
freewnn_conversion(IMContextFreeWnn* cf) {
  w_char* wcbuf = NULL;
  gchar kanjibuf[BUFSIZ];
  gint current_seg = 0;

  if( strlen(cf->preedit_buf) == 0 )
    return;

  if( cf->kanji_on == FALSE ) {
    wcbuf = utf8_to_wchar(cf->preedit_buf);
    jl_ren_conv(cf->wnnbuf, wcbuf, 0, -1, WNN_NO_USE);
    jl_zenkouho(cf->wnnbuf, 0, WNN_NO_USE, WNN_UNIQ_KNJ);
    cf->kanji_on = TRUE;
    cf->prefix_utf8_len = 0;
    cf->kanji_utf8_len = 0;
  } else {
    current_seg = jl_zenkouho_bun(cf->wnnbuf);
/* CandWin test */
{
  int i = 0;
  GSList* cand_slist = NULL;
  int max_index = 0;
  int index = 0;
  int current_index = jl_c_zenkouho(cf->wnnbuf);
  jl_set_jikouho(cf->wnnbuf, 0);
  jl_previous(cf->wnnbuf);
  max_index = jl_c_zenkouho(cf->wnnbuf);

  /* If there is no candidate, we don't need candwin */
  if( max_index == 0 )
    return;

  for(i=0;i<max_index;i++) {
    jl_set_jikouho(cf->wnnbuf, i);
    memset(kanjibuf, 0, sizeof(gchar)*BUFSIZ);
    jl_utf8_get_kanji(cf->wnnbuf, current_seg, current_seg+1, kanjibuf);
    cand_slist = g_slist_append(cand_slist, g_strdup(kanjibuf));
  }
  if( cf->candwin->cand_slist ) {
    g_slist_free(cf->candwin->cand_slist);
    cf->candwin->cand_slist = NULL;
  }
  candwin_set_candlist(cf->candwin, cand_slist);
  g_signal_connect(cf->candwin->window, "hide", G_CALLBACK(candwin_hide_cb), cf);
  candwin_set_index(cf->candwin, (guint)current_index);
  gtk_widget_show_all(cf->candwin->window);
  return;
}
//    jl_next(cf->wnnbuf);
  }

  memset(kanjibuf, 0, sizeof(gchar)*BUFSIZ);
  cf->kanji_len = jl_utf8_get_kanji(cf->wnnbuf, 0, -1, kanjibuf);
  memset(cf->preedit_buf, 0, sizeof(gchar)*BUFSIZ);
  strcpy(cf->preedit_buf, kanjibuf);

  cf->prefix_utf8_len = get_prefix_utf8_len(cf, current_seg);
  jl_utf8_get_kanji(cf->wnnbuf, current_seg, current_seg+1, kanjibuf);
  cf->kanji_utf8_len = strlen(kanjibuf);
}

static int
jl_utf8_get_kanji(struct wnn_buf* wnnbuf, int bun_no, int bun_no2, gchar* area) {
  w_char kanjibuf[BUFSIZ];
  gint ret = 0;
  gchar* utf8 = NULL;

  memset(kanjibuf, 0, BUFSIZ);
  ret = jl_get_kanji(wnnbuf, bun_no, bun_no2, kanjibuf);
  kanjibuf[ret] = 0;
  utf8 = wchar_to_utf8(kanjibuf);
  strcpy(area, utf8);
  
  g_free(utf8);
  return ret;
}

static gchar*
get_full_width(guint keyval) {
  guint i = 0;
  while(full_width_table[i].utf8 != NULL) {
    if( keyval == full_width_table[i].keyval )
      return full_width_table[i].utf8;
    i++;
  }

  return NULL;
}

/*
 * Get offset to current bunsetsu in UTF-8 char length.
 *   If the current segment is unknown, set cseg to -1, and this func
 *   will detect inside.
 */
static gint
get_prefix_utf8_len(IMContextFreeWnn* cf, gint cseg) {
   gint current_seg = (cseg < 0) ? jl_zenkouho_bun(cf->wnnbuf) : cseg;
   gint prefix_utf8_len = 0;
   gchar kanjibuf[BUFSIZ];

   if( current_seg > 0 ) {
      jl_utf8_get_kanji(cf->wnnbuf, 0, current_seg, kanjibuf);
      prefix_utf8_len = strlen(kanjibuf);
   } else {
      prefix_utf8_len = 0;
   }

   return prefix_utf8_len;
}

static void
goto_segment(IMContextFreeWnn* cf, gint segment) {
    gchar kanjibuf[BUFSIZ];
    gint nofseg = jl_bun_suu(cf->wnnbuf);
    gint current_segment = segment;

    /* If number of segment is 1, there is no need to move segment. */
    if( nofseg < 2 ) {
      return;
    }

    if( current_segment < 0 )
      current_segment = nofseg - 1;

    if( current_segment >= nofseg )
      current_segment = 0;

    jl_zenkouho(cf->wnnbuf, current_segment, WNN_NO_USE, WNN_UNIQ_KNJ);
    
    cf->prefix_utf8_len = get_prefix_utf8_len(cf, current_segment);
    jl_utf8_get_kanji(cf->wnnbuf, current_segment, current_segment+1, kanjibuf);
    cf->kanji_utf8_len = strlen(kanjibuf);
    g_signal_emit_by_name(cf, "preedit_changed");
}

static void
move_modewin(IMContextFreeWnn* cf) {
  GdkWindow* toplevel_gdk = cf->client_window;
  GdkScreen* screen;
  GdkWindow* root_window;
  GtkWidget* toplevel;
  GdkRectangle rect;
  GtkRequisition requisition;
  gint y;
  gint height;

  if( !toplevel_gdk )
    return;

  screen = gdk_drawable_get_screen (toplevel_gdk);
  root_window = gdk_screen_get_root_window(screen);

  while(TRUE) {
    GdkWindow* parent = gdk_window_get_parent(toplevel_gdk);
    if( parent == root_window )
      break;
    else
      toplevel_gdk = parent;
  }

  gdk_window_get_user_data(toplevel_gdk, (gpointer*)&toplevel);
  if( !toplevel )
    return;

  height = gdk_screen_get_height (gtk_widget_get_screen (toplevel));
  gdk_window_get_frame_extents (toplevel->window, &rect);
  freewnn_modewin_size_request(cf->modewin, &requisition);

  if (rect.y + rect.height + requisition.height < height)
    y = rect.y + rect.height;
  else
    y = height - requisition.height;

  freewnn_modewin_move(cf->modewin, rect.x, y);
}

static void
update_modewin(IMContextFreeWnn* cf) {
  if( cf->kanji_on ) {
    /* This char should not be translated. */
    freewnn_modewin_set_text(cf->modewin, "漢");
    freewnn_modewin_show(cf->modewin);
    move_modewin(cf);
  } else if( cf->ja_mode ) {
    /* This char should not be translated. */
    freewnn_modewin_set_text(cf->modewin, "あ");
    freewnn_modewin_show(cf->modewin);
    move_modewin(cf);
  } else {
    freewnn_modewin_hide(cf->modewin);
  }
}

/* Commit all and clear the preedit buffer */
static void
im_freewnn_commit(IMContextFreeWnn* cf) {
  g_signal_emit_by_name(cf, "commit", cf->preedit_buf);
  memset(cf->preedit_buf, 0, sizeof(gchar)*BUFSIZ);
  g_signal_emit_by_name(cf, "preedit_changed");
  cf->kanji_on = FALSE;
  cf->kanji_utf8_len = 0;
  update_modewin(cf);
}
