/*
 * convtable.h: Romaji kana converter table text c header
 * (This file is converted from converter.txt with converter.pl)
 *
 * Copyright (C) 2001-2004 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _CONVTABLE_H
#define _CONVTABLE_H

typedef struct _romapair {
  char* roma;
  char* kana;
} romapair;

romapair kanatable[] = {
  { "a", "あ" },
  { "i", "い" },
  { "u", "う" },
  { "e", "え" },
  { "o", "お" },

  { "ka", "か" },
  { "ki", "き" },
  { "ku", "く" },
  { "ke", "け" },
  { "ko", "こ" },

  { "sa", "さ" },
  { "si", "し" },
  { "su", "す" },
  { "se", "せ" },
  { "so", "そ" },

  { "ta", "た" },
  { "ti", "ち" },
  { "tu", "つ" },
  { "te", "て" },
  { "to", "と" },

  { "na", "な" },
  { "ni", "に" },
  { "nu", "ぬ" },
  { "ne", "ね" },
  { "no", "の" },

  { "ha", "は" },
  { "hi", "ひ" },
  { "hu", "ふ" },
  { "he", "へ" },
  { "ho", "ほ" },

  { "ma", "ま" },
  { "mi", "み" },
  { "mu", "む" },
  { "me", "め" },
  { "mo", "も" },

  { "ya", "や" },
  { "yi", "い" },
  { "yu", "ゆ" },
  { "ye", "いぇ" },
  { "yo", "よ" },

  { "ra", "ら" },
  { "ri", "り" },
  { "ru", "る" },
  { "re", "れ" },
  { "ro", "ろ" },

  { "wa", "わ" },
  { "wi", "うぃ" },
  { "wu", "う" },
  { "we", "うぇ" },
  { "wo", "を" },

  { "ga", "が" },
  { "gi", "ぎ" },
  { "gu", "ぐ" },
  { "ge", "げ" },
  { "go", "ご" },

  { "za", "ざ" },
  { "zi", "じ" },
  { "zu", "ず" },
  { "ze", "ぜ" },
  { "zo", "ぞ" },

  { "da", "だ" },
  { "di", "ぢ" },
  { "du", "づ" },
  { "de", "で" },
  { "do", "ど" },

  { "ba", "ば" },
  { "bi", "び" },
  { "bu", "ぶ" },
  { "be", "べ" },
  { "bo", "ぼ" },

  { "va", "う゛ぁ" },
  { "vi", "う゛ぃ" },
  { "vu", "う゛" },
  { "ve", "う゛ぇ" },
  { "vo", "う゛ぉ" },

  { "pa", "ぱ" },
  { "pi", "ぴ" },
  { "pu", "ぷ" },
  { "pe", "ぺ" },
  { "po", "ぽ" },

  { "fa", "ふぁ" },
  { "fi", "ふぃ" },
  { "fu", "ふ" },
  { "fe", "ふぇ" },
  { "fo", "ふぉ" },

  { "ja", "じゃ" },
  { "ji", "じ" },
  { "ju", "じゅ" },
  { "je", "じぇ" },
  { "jo", "じょ" },

  { "kya", "きゃ" },
  { "kyi", "きぃ" },
  { "kyu", "きゅ" },
  { "kye", "きぇ" },
  { "kyo", "きょ" },

  { "gya", "ぎゃ" },
  { "gyi", "ぎぃ" },
  { "gyu", "ぎゅ" },
  { "gye", "ぎぇ" },
  { "gyo", "ぎょ" },

  { "sya", "しゃ" },
  { "syi", "しぃ" },
  { "syu", "しゅ" },
  { "sye", "しぇ" },
  { "syo", "しょ" },

  { "zya", "じゃ" },
  { "zyi", "じぃ" },
  { "zyu", "じゅ" },
  { "zye", "じぇ" },
  { "zyo", "じょ" },

  { "tya", "ちゃ" },
  { "tyi", "ちぃ" },
  { "tyu", "ちゅ" },
  { "tye", "ちぇ" },
  { "tyo", "ちょ" },

  { "dya", "ぢゃ" },
  { "dyi", "ぢぃ" },
  { "dyu", "ぢゅ" },
  { "dye", "ぢぇ" },
  { "dyo", "ぢょ" },

  { "nya", "にゃ" },
  { "nyi", "にぃ" },
  { "nyu", "にゅ" },
  { "nye", "にぇ" },
  { "nyo", "にょ" },
  { "hya", "ひゃ" },
  { "hyi", "ひぃ" },
  { "hyu", "ひゅ" },
  { "hye", "ひぇ" },
  { "hyo", "ひょ" },

  { "bya", "びゃ" },
  { "byi", "びぃ" },
  { "byu", "びゅ" },
  { "bye", "びぇ" },
  { "byo", "びょ" },

  { "pya", "ぴゃ" },
  { "pyi", "ぴぃ" },
  { "pyu", "ぴゅ" },
  { "pye", "ぴぇ" },
  { "pyo", "ぴょ" },

  { "mya", "みゃ" },
  { "myi", "みぃ" },
  { "myu", "みゅ" },
  { "mye", "みぇ" },
  { "myo", "みょ" },

  { "rya", "りゃ" },
  { "ryi", "りぃ" },
  { "ryu", "りゅ" },
  { "rye", "りぇ" },
  { "ryo", "りょ" },

  { "tsa", "つぁ" },
  { "tsi", "つぃ" },
  { "tsu", "つ" },
  { "tse", "つぇ" },
  { "tso", "つぉ" },

  { "sha", "しゃ" },
  { "shi", "し" },
  { "shu", "しゅ" },
  { "she", "しぇ" },
  { "sho", "しょ" },

  { "cha", "ちゃ" },
  { "chi", "ち" },
  { "chu", "ちゅ" },
  { "che", "ちぇ" },
  { "cho", "ちょ" },

  { "jya", "じゃ" },
  { "jyi", "じぃ" },
  { "jyu", "じゅ" },
  { "jye", "じぇ" },
  { "jyo", "じょ" },

  { "tha", "てゃ" },
  { "thi", "てぃ" },
  { "thu", "てゅ" },
  { "the", "てぇ" },
  { "tho", "てょ" },

  { "dha", "でゃ" },
  { "dhi", "でぃ" },
  { "dhu", "でゅ" },
  { "dhe", "でぇ" },
  { "dho", "でょ" },

  { "la", "ぁ" },
  { "le", "ぇ" },
  { "li", "ぃ" },
  { "lo", "ぉ" },
  { "ltu", "っ" },
  { "ltsu", "っ" },
  { "lu", "ぅ" },
  { "lya", "ゃ" },
  { "lye", "ぇ" },
  { "lyi", "ぃ" },
  { "lyo", "ょ" },
  { "lyu", "ゅ" },

  { "xa", "ぁ" },
  { "xe", "ぇ" },
  { "xi", "ぃ" },
  { "xo", "ぉ" },
  { "xtu", "っ" },
  { "xtsu", "っ" },
  { "xu", "ぅ" },
  { "xya", "ゃ" },
  { "xye", "ぇ" },
  { "xyi", "ぃ" },
  { "xyo", "ょ" },
  { "xyu", "ゅ" },

  { "nn", "ん" },

  { "nk", "んk" },
  { "ns", "んs" },
  { "nt", "んt" },
  { "nh", "んh" },
  { "nm", "んm" },
  { "nr", "んr" },
  { "nw", "んw" },
  { "ng", "んg" },
  { "nz", "んz" },
  { "nd", "んd" },
  { "nb", "んb" },

  { "kk", "っk" },
  { "ss", "っs" },
  { "tt", "っt" },
  { "hh", "っh" },
  { "mm", "っm" },
  { "yy", "っy" },
  { "rr", "っr" },
  { "ww", "っw" },
  { "gg", "っg" },
  { "zz", "っz" },
  { "dd", "っd" },
  { "bb", "っb" },
  { "pp", "っp" },
  { "cc", "っc" },
  { "ff", "っf" },
  { "jj", "っj" },
  { "qq", "っq" },
  { "vv", "っv" },
  { "tch", "っch" },

  { "-", "ー" },
  { "@", "゛" },
};

#endif /* _CONVTABLE_H */
