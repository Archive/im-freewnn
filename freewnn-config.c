/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include "freewnn-config.h"
#include "im-freewnn-intl.h"

struct _FreeWnnConfig {
  GtkWidget* dialog;
  GtkWidget* dialog_vbox;
  GtkWidget* notebook;
  GtkWidget* kinput2_radiobutton;
  GtkWidget* xemacs_radiobutton;
  GSList* radiobutton_group;
  GtkWidget* label;
  GtkWidget* dialog_action_area;
  GtkWidget* cancelbutton;
  GtkWidget* okbutton;

  FreeWnnFuncTable** functablep;
};

FreeWnnConfig*
freewnn_config_new(FreeWnnFuncTable** functablep) {
  FreeWnnConfig* config = g_new0(FreeWnnConfig, 1);
  GtkWidget* vbox = NULL;

  config->dialog = gtk_dialog_new ();
  gtk_window_set_title(GTK_WINDOW(config->dialog), _("im-freewnn configure"));
  config->dialog_vbox = GTK_DIALOG(config->dialog)->vbox;

  config->notebook = gtk_notebook_new();
  gtk_box_pack_start(GTK_BOX(config->dialog_vbox), config->notebook, TRUE, TRUE, 5);
  gtk_container_set_border_width(GTK_CONTAINER(config->notebook), 5);

  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(config->notebook), vbox);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);

  config->kinput2_radiobutton = gtk_radio_button_new_with_mnemonic(NULL, _("_Kinput2 style"));
  gtk_box_pack_start(GTK_BOX(vbox), config->kinput2_radiobutton, FALSE, FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(config->kinput2_radiobutton), 10);
  GTK_WIDGET_SET_FLAGS(config->kinput2_radiobutton, GTK_CAN_DEFAULT);
  gtk_widget_grab_default(config->kinput2_radiobutton);
  if( *functablep == kinput2style_functable ) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(config->kinput2_radiobutton), TRUE);
  }

  gtk_radio_button_set_group(GTK_RADIO_BUTTON(config->kinput2_radiobutton), config->radiobutton_group);
  config->radiobutton_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(config->kinput2_radiobutton));

  config->xemacs_radiobutton = gtk_radio_button_new_with_mnemonic(NULL, _("_XEmacs style"));
  gtk_box_pack_start(GTK_BOX(vbox), config->xemacs_radiobutton, FALSE, FALSE, 0);
  if( *functablep == eggstyle_functable ) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(config->xemacs_radiobutton), TRUE);
  }

  gtk_container_set_border_width(GTK_CONTAINER(config->xemacs_radiobutton), 10);
  gtk_radio_button_set_group(GTK_RADIO_BUTTON(config->xemacs_radiobutton), config->radiobutton_group);
  config->radiobutton_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(config->xemacs_radiobutton));

  config->label = gtk_label_new(_("Key Binding Style"));
  gtk_notebook_set_tab_label(GTK_NOTEBOOK(config->notebook), gtk_notebook_get_nth_page(GTK_NOTEBOOK(config->notebook), 0), config->label);

  config->dialog_action_area = GTK_DIALOG(config->dialog)->action_area;
  gtk_button_box_set_layout(GTK_BUTTON_BOX(config->dialog_action_area), GTK_BUTTONBOX_END);
  config->cancelbutton = gtk_button_new_from_stock("gtk-cancel");
  gtk_dialog_add_action_widget(GTK_DIALOG(config->dialog), config->cancelbutton, GTK_RESPONSE_CANCEL);
  GTK_WIDGET_SET_FLAGS(config->cancelbutton, GTK_CAN_DEFAULT);

  config->okbutton = gtk_button_new_from_stock("gtk-ok");
  gtk_dialog_add_action_widget(GTK_DIALOG(config->dialog), config->okbutton, GTK_RESPONSE_OK);
  GTK_WIDGET_SET_FLAGS(config->okbutton, GTK_CAN_DEFAULT);

  gtk_widget_show_all(GTK_DIALOG(config->dialog)->vbox);

  config->functablep = functablep;

  return config;
}

void
freewnn_config_run(FreeWnnConfig* config) {
  if( GTK_WIDGET_VISIBLE(config->dialog) )
    return;

  gtk_dialog_run(GTK_DIALOG(config->dialog));
  gtk_widget_hide(config->dialog);
  if( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(config->kinput2_radiobutton))) {
    *config->functablep = kinput2style_functable;
  } else {
    *config->functablep = eggstyle_functable;
  }
}

void
freewnn_config_finalize(FreeWnnConfig* config) {
  /* I'm not sure destroy codes should be in this finalize funtion. */
  gtk_widget_destroy(config->dialog);
  g_free(config);
}
