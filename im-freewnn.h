/* im-freewnn - immodule for FreeWnn
 *
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#ifndef _IM_FREEWNN_H
#define _IM_FREEWNN_H

#include "keytable.h"

#include <gtk/gtk.h>
#include <gtk/gtkimmodule.h>
#include <jllib.h>

#include "freewnn-modewin.h"
#include "freewnn-utils.h"
#include "candwin.h"

#define IM_CONTEXT_TYPE_FREEWNN (type_freewnn)
#define IM_CONTEXT_FREEWNN(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), IM_CONTEXT_TYPE_FREEWNN, IMContextFreeWnn))
#define IM_CONTEXT_IS_FREEWNN(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IM_CONTEXT_TYPE_FREEWNN))

typedef struct _IMContextFreeWnn {
  GtkIMContext parent;
  GdkWindow* client_window;
  gchar* preedit_buf;
  gboolean ja_mode;
  struct wnn_buf* wnnbuf; /* Wnn Buffer */
  gboolean kanji_on;

  gint kanji_len; /* Return value of jl_get_kanji, in euc-jp chars */
  gint prefix_utf8_len; /* Current segment offset in UTF-8 bytes */
  gint kanji_utf8_len; /* Current segment length in UTF-8 bytes */

  /* Mode Window */
  FreeWnnModeWin* modewin;

  /* Candidate Window */
  CandWin* candwin;

  FreeWnnFuncTable* functable;
} IMContextFreeWnn;

typedef struct _IMContextFreeWnnClass {
  GtkIMContextClass parent_class;
} IMContextFreeWnnClass;

#endif /* _IM_FREEWNN_H */
