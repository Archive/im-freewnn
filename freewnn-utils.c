/* im-freewnn - FreeWnn immodule
 *
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "freewnn-utils.h"
#include "kata2hkata.h"

w_char*
char2wchar(unsigned char* str) {
  w_char* result = NULL;
  int i=0;
  int j=0;
  int count = 0;

  if( str == NULL ) return NULL;
  if( *str == '\0' ) return NULL;

  for(i=0;i<strlen(str);i++) {
    count++;
    if( str[i] & 0x80 )
      i++;
  }

  result = (w_char*)calloc(count+1, sizeof(w_char));
  for(i=0;i<strlen(str);i++) {
    if( str[i] & 0x80 ) {
      result[j] =  str[i];
      result[j] = (result[j] << 8 ) | str[++i];
    } else {
      result[j] = str[i];
    }
    j++;
  }

#if 0
  {
    for(i=0;i<count;i++) {
      printf("0x%04x\n", result[i]);
    }
  }
#endif

  return result;
}

w_char*
utf8_to_wchar(gchar* utf8) {
  gchar* euc = im_convert(utf8, "EUC-JP", "UTF-8");
  w_char* result = char2wchar(euc);

  g_free(euc);

  return result;
}

unsigned char*
wchar2char(w_char* wstr) {
  char* result = NULL;
  int i;
  int j;
  int count = 0;

  if( wstr == NULL ) return NULL;

  if( *wstr == 0 ) return NULL;

  for(i=0;wstr[i] != 0;i++) 
    count++;

  result = (unsigned char*)calloc(count*2+1, sizeof(unsigned char));
  j=0;
  for(i=0;i<count;i++) {
    if( wstr[i] & 0xff00 ) {
      result[j++] = wstr[i] >> 8;
    }
    result[j++] = wstr[i] & 0xff;
  }

  return result;
}

/* Convrt wnn's w_char to utf8 string */
gchar*
wchar_to_utf8(w_char* wstr) {
  gchar* euc = wchar2char(wstr);
  gchar* utf8 = im_convert(euc, "UTF-8", "EUC-JP");
  g_free(euc);

  return utf8;
}

gchar *
im_convert (const gchar* str, const gchar* to, const gchar* from)
{
  GError *error = NULL;
  gchar *result = NULL;
  gsize bytes_read = 0, bytes_written = 0;
                                                                                
  g_assert(str != NULL);
                                                                                
  result = g_convert (str, -1,
                      to, from,
                      &bytes_read, &bytes_written, &error);
  if (!result)
    {
      g_warning ("Error converting text from %s to %s: %s\n", from, to, error->message);
      g_print("%02X %02X\n", ((guchar*)str)[bytes_read],((guchar*)str)[bytes_read+1]);
      g_print("Error: bytes_read: %d\n", bytes_read);
      g_print("Error: bytes_written: %d\n", bytes_written);
      g_error_free (error);
    }

  return result;
}

gchar*
im_hira2kata(gchar* text) {
  gint i = 0;
  gchar buf[7];
  gchar* buffer = NULL;

  g_return_val_if_fail(text != NULL, NULL);
  g_return_val_if_fail(*text != '\0', NULL);

  buffer = g_strdup(text);
                                                                                
  for(i=0;i<g_utf8_strlen(buffer, -1);i++) {
    gchar* cursor = g_utf8_offset_to_pointer(buffer, i);
    gunichar uchar = g_utf8_get_char(cursor);
    if( (uchar >= 0x3041 && uchar <= 0x3095) || uchar == 0x309D || uchar == 0x309E ) {
       g_unichar_to_utf8(uchar+0x60, buf);
       cursor[0] = buf[0];
       cursor[1] = buf[1];
       cursor[2] = buf[2];
    }
  }
                                                                                
  return buffer;
}

gchar*
im_kata2hira(gchar* text) {
  gint i = 0;
  gchar buf[7];
  gchar* buffer = NULL;

  g_return_val_if_fail(text != NULL, NULL);
  g_return_val_if_fail(*text != '\0', NULL);

  buffer = g_strdup(text);
                                                                                
  for(i=0;i<g_utf8_strlen(buffer, -1);i++) {
    gchar* cursor = g_utf8_offset_to_pointer(buffer, i);
    gunichar uchar = g_utf8_get_char(cursor);
    if( (uchar >= 0x30A1 && uchar <= 0x30F5) || uchar == 0x30FD || uchar == 0x30FE ) {
       g_unichar_to_utf8(uchar-0x60, buf);
       cursor[0] = buf[0];
       cursor[1] = buf[1];
       cursor[2] = buf[2];
    }
  }
                                                                                
  return buffer;
}

gchar*
im_kata2hkata(gchar* text) {
  gint i=0;
  gint j=0;
  gchar* buf = NULL;
  gchar* result = NULL;
                                                                                
  g_return_val_if_fail(text != NULL, NULL);
  g_return_val_if_fail(*text != '\0', NULL);
                                                                                
  buf = g_new0(gchar, strlen(text)*2+1);
                                                                                
  for(i=0;i<g_utf8_strlen(text, -1);i++) {
    gboolean conv_hit = FALSE;
    gchar* cursor = g_utf8_offset_to_pointer(text, i);

    for(j=0;j<G_N_ELEMENTS(twoway_table);j++) {
      const gchar* kata = twoway_table[j].kata;
      if( strncmp(cursor, kata, strlen(kata)) == 0 ) {
        strcat(buf, twoway_table[j].hkata);
        conv_hit = TRUE;
        break;
      }
    }
    if( !conv_hit ) {
      strncat(buf, cursor, g_utf8_offset_to_pointer(cursor, 1)-cursor);
    }
  }
                                                                                
  result = g_strndup(buf, strlen(buf));
  g_free(buf);
                                                                                
  return result;
}

gchar*
im_hkata2kata(gchar* text) {
  gint i=0;
  gint j=0;
  gchar* buf = NULL;
  gchar* result = NULL;
                                                                                
  g_return_val_if_fail(text != NULL, NULL);
  g_return_val_if_fail(*text != '\0', NULL);
                                                                                
  buf = g_new0(gchar, strlen(text)*3+1);

  for(i=0;i<g_utf8_strlen(text, -1);i++) {
    gboolean conv_hit = FALSE;
    gchar* cursor = g_utf8_offset_to_pointer(text, i);

    /* Try 2 chars match - voice mark */
    if( g_utf8_strlen(cursor, -1) >= 2 ) {
      gchar* second = g_utf8_offset_to_pointer(cursor, 1);
      if( !strncmp(second, "ﾞ", strlen("ﾞ"))
          || !strncmp(second, "ﾟ", strlen("ﾟ") )) {
        gchar* twochar = g_strndup(cursor, g_utf8_offset_to_pointer(cursor, 2)-cursor);
        for(j=0;j<G_N_ELEMENTS(twoway_table);j++) {
          const gchar* hkata = twoway_table[j].hkata;
          if( !strcmp(twochar, hkata) ) {
            strcat(buf, twoway_table[j].kata);
            conv_hit = TRUE;
            i++; /* Increment for voice mark */
            break;
          }
        }
        g_free(twochar);
      }
    }

    /* 1 char match */
    if( !conv_hit ) {
      for(j=0;j<G_N_ELEMENTS(twoway_table);j++) {
        const gchar* hkata = twoway_table[j].hkata;
        if( strncmp(cursor, hkata, strlen(hkata)) == 0 ) {
          strcat(buf, twoway_table[j].kata);
          conv_hit = TRUE;
          break;
        }
      }
    }

    if( !conv_hit ) {
      strncat(buf, cursor, g_utf8_offset_to_pointer(cursor, 1)-cursor);
    }
  }
                                                                                
  result = g_strndup(buf, strlen(buf));
  g_free(buf);
                                                                                
  return result;
}

/* Convert all kinds of kana to hiragana */
gchar*
im_anykana2hira(gchar* text) {
  gchar* hira = NULL;
  gchar* kata = NULL;

  g_return_val_if_fail(text != NULL, NULL);
  g_return_val_if_fail(*text != '\0', NULL);

  kata = im_hkata2kata(text);
  hira = im_kata2hira(kata);

  g_free(kata);

  return hira;
}

/* Convert all kinds of kana to katakana */
gchar*
im_anykana2kata(gchar* text) {
  gchar* kata = NULL;
  gchar* result = NULL;

  g_return_val_if_fail(text != NULL, NULL);
  g_return_val_if_fail(*text != '\0', NULL);

  kata = im_hkata2kata(text);
  result = im_hira2kata(kata);

  g_free(kata);

  return result;
}

/* Convert all kinds of kana to half katakana */
gchar*
im_anykana2hkata(gchar* text) {
  gchar* hira = NULL;
  gchar* kata = NULL;
  gchar* result = NULL;

  g_return_val_if_fail(text != NULL, NULL);
  g_return_val_if_fail(*text != '\0', NULL);

  kata = im_hira2kata(text);
  result = im_kata2hkata(kata);

  g_free(kata);

  return result;
}
