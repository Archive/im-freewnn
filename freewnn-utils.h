/* im-freewnn - FreeWnn immodule
 *
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <jllib.h>
#include <glib.h>

w_char* char2wchar(unsigned char* str);
w_char* utf8_to_wchar(gchar* utf8);
unsigned char* wchar2char(w_char* wstr);
gchar* wchar_to_utf8(w_char* wstr);
gchar* im_convert(const gchar* str, const gchar* to, const gchar* from);
gchar* im_hira2kata(gchar* text);
gchar* im_kata2hira(gchar* text);
gchar* im_kata2hkata(gchar* text);
gchar* im_hkata2kata(gchar* text);
gchar* im_anykana2hira(gchar* text);
gchar* im_anykana2kata(gchar* text);
gchar* im_anykana2hkata(gchar* text);
