/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include "freewnn-modewin.h"
#include "freewnn-utils.h"
#include "freewnn-utils-ui.h"
#include "im-freewnn-intl.h"
#include "freewnn-config.h"

static void freewnn_modewin_enter(GtkWidget* widget, GdkEvent* event, FreeWnnModeWin* modewin);
static void freewnn_modewin_leave(GtkWidget* widget, GdkEvent* event, FreeWnnModeWin* modewin);
static void freewnn_modewin_regist_word(GtkWidget* widget, FreeWnnModeWin* modewin);
static void freewnn_modewin_config_cb(GtkWidget* widget, FreeWnnModeWin* modewin);

FreeWnnModeWin*
freewnn_modewin_new(struct wnn_buf *buf, FreeWnnFuncTable** functablep) {
  FreeWnnModeWin* modewin = g_new0(FreeWnnModeWin, 1);
  GtkTooltips* modetip = gtk_tooltips_new();
  GtkTooltips* configtip = gtk_tooltips_new();

  modewin->win0 = gtk_window_new(GTK_WINDOW_POPUP);
  gtk_window_set_accept_focus(GTK_WINDOW(modewin->win0), TRUE);
  modewin->label0 = gtk_label_new("");

  modewin->win1 = gtk_window_new(GTK_WINDOW_POPUP);
  gtk_window_set_accept_focus(GTK_WINDOW(modewin->win1), TRUE);
  modewin->hbox1 = gtk_hbox_new(FALSE, 0);
  modewin->mode_button1 = gtk_button_new_with_label("");
  gtk_tooltips_set_tip(modetip, modewin->mode_button1, _("Current Input Mode"), NULL);
  modewin->config_button1 = gtk_button_new_with_label("設");
  gtk_tooltips_set_tip(configtip, modewin->config_button1, _("Input Method Config Dialog"), NULL);
  g_signal_connect(modewin->config_button1, "clicked", G_CALLBACK(freewnn_modewin_config_cb), modewin);

  modewin->regist_button1 = gtk_button_new_with_label("登");
  gtk_tooltips_set_tip(configtip, modewin->regist_button1, _("Register New Word"), NULL);
  g_signal_connect(modewin->regist_button1, "clicked", G_CALLBACK(freewnn_modewin_regist_word), modewin);

  modewin->label_text = g_strdup("");

  gtk_container_add(GTK_CONTAINER(modewin->win0), modewin->label0);

  gtk_container_add(GTK_CONTAINER(modewin->win1), modewin->hbox1);
  gtk_container_set_border_width(GTK_CONTAINER(modewin->win1), 2);
  gtk_box_pack_start(GTK_BOX(modewin->hbox1), modewin->mode_button1, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(modewin->hbox1), modewin->config_button1, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(modewin->hbox1), modewin->regist_button1, FALSE, FALSE, 0);

  modewin->enter_hid = g_signal_connect(modewin->win0, "enter_notify_event", G_CALLBACK(freewnn_modewin_enter), modewin);
  modewin->leave_hid = g_signal_connect(modewin->win1, "leave_notify_event", G_CALLBACK(freewnn_modewin_leave), modewin);

  modewin->handlers_blocked = FALSE;

  modewin->buf = buf;

  modewin->register_win = freewnn_register_win_new(buf);

  modewin->config = freewnn_config_new(functablep);
  return modewin;
}

void
freewnn_modewin_finalize(FreeWnnModeWin* modewin) {
  g_signal_handler_disconnect(modewin->win0, modewin->enter_hid);
  g_signal_handler_disconnect(modewin->win1, modewin->leave_hid);

  freewnn_register_win_finalize(modewin->register_win);
  freewnn_config_finalize(modewin->config);

  gtk_widget_destroy(modewin->win0);
  gtk_widget_destroy(modewin->win1);

  g_free(modewin->label_text);

  g_free(modewin);
}

void
freewnn_modewin_set_text(FreeWnnModeWin* modewin, gchar* text) {
  gchar* bracetext = NULL;

  g_free(modewin->label_text);
  modewin->label_text = g_strdup(text);

  /* Just add braces to the text */
  bracetext = g_strconcat("[", text, "]", NULL);

  gtk_label_set_text(GTK_LABEL(modewin->label0), bracetext);
  gtk_button_set_label(GTK_BUTTON(modewin->mode_button1), text);

  g_free(bracetext);
}

void
freewnn_modewin_show(FreeWnnModeWin* modewin) {
  if( modewin->handlers_blocked ) {
    g_signal_handler_unblock(modewin->win1, modewin->leave_hid);
    g_signal_handler_unblock(modewin->win0, modewin->enter_hid);
    modewin->handlers_blocked = FALSE;
  }
  gtk_widget_show_all(modewin->win0);
}

void
freewnn_modewin_hide(FreeWnnModeWin* modewin) {
  g_assert(modewin->handlers_blocked == FALSE);
  g_signal_handler_block(modewin->win1, modewin->leave_hid);
  g_signal_handler_block(modewin->win0, modewin->enter_hid);
  modewin->handlers_blocked = TRUE;

  gtk_widget_hide_all(modewin->win0);
  gtk_widget_hide_all(modewin->win1);
}

void
freewnn_modewin_move(FreeWnnModeWin* modewin, gint x, gint y) {
  modewin->x = x;
  modewin->y = y;

  gtk_window_move(GTK_WINDOW(modewin->win0), x, y);
  gtk_window_move(GTK_WINDOW(modewin->win1), x, y);
}

void
freewnn_modewin_size_request(FreeWnnModeWin* modewin, GtkRequisition* req) {
  gtk_widget_size_request(modewin->win0, req);
  gtk_widget_size_request(modewin->win1, req);
}

static void
freewnn_modewin_enter(GtkWidget* widget, GdkEvent* event, FreeWnnModeWin* modewin) {
  gtk_widget_hide_all(modewin->win0);
  gtk_widget_show_all(modewin->win1);
}

static void
freewnn_modewin_leave(GtkWidget* widget, GdkEvent* event, FreeWnnModeWin* modewin) {
  gtk_widget_hide_all(modewin->win1);
  gtk_widget_show_all(modewin->win0);
}

static void
freewnn_modewin_regist_word(GtkWidget* widget, FreeWnnModeWin* modewin) {
  freewnn_register_win_show(modewin->register_win);
}

static void
freewnn_modewin_config_cb(GtkWidget* widget, FreeWnnModeWin* modewin) {
  freewnn_config_run(modewin->config);
}
