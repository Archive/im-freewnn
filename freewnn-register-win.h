/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#ifndef _FREEWNN_REGIST_WIN
#define _FREEWNN_REGIST_WIN

#include <gtk/gtk.h>
#include <jllib.h>

typedef struct _FreeWnnRegisterWin FreeWnnRegisterWin;

FreeWnnRegisterWin* freewnn_register_win_new(struct wnn_buf* buf);
void freewnn_register_win_show(FreeWnnRegisterWin* win);
void freewnn_register_win_hide(FreeWnnRegisterWin* win);
void freewnn_register_win_finalize(FreeWnnRegisterWin* win);

#endif /* _FREEWNN_REGIST_WIN */
