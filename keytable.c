/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include "keytable.h"

FreeWnnFuncTable kinput2style_functable[] = {
  { MASK_CONTROL, GDK_b, FREEWNN_KEY_PREV_SEGMENT },
  { MASK_CONTROL, GDK_f, FREEWNN_KEY_NEXT_SEGMENT },
  { MASK_NONE,    GDK_Left, FREEWNN_KEY_PREV_SEGMENT },
  { MASK_NONE,    GDK_Right, FREEWNN_KEY_NEXT_SEGMENT },
  { MASK_CONTROL, GDK_a, FREEWNN_KEY_FIRST_SEGNENT },
  { MASK_CONTROL, GDK_e, FREEWNN_KEY_LAST_SEGMENT },
  { MASK_CONTROL, GDK_o, FREEWNN_KEY_SEGMENT_LONGER },
  { MASK_CONTROL, GDK_i, FREEWNN_KEY_SEGMENT_SHORTER },
  { MASK_SHIFT, GDK_Right, FREEWNN_KEY_SEGMENT_LONGER },
  { MASK_SHIFT, GDK_Left, FREEWNN_KEY_SEGMENT_SHORTER },
  { MASK_CONTROL, GDK_w, FREEWNN_KEY_SEGMENT_ALLLIST },
  //FREEWNN_KEY_???
  { MASK_CONTROL, GDK_d, FREEWNN_KEY_DELETE_UNCOMMITTED },
  { MASK_NONE, GDK_F8, FREEWNN_KEY_TO_HIRAGANA },
  { MASK_ALT, GDK_h, FREEWNN_KEY_TO_HIRAGANA },
  { MASK_NONE, GDK_F7, FREEWNN_KEY_TO_KATAKANA },
  { MASK_ALT, GDK_k, FREEWNN_KEY_TO_KATAKANA },
  { MASK_NONE, GDK_F6, FREEWNN_KEY_TO_HALF_KATAKANA },
  { MASK_NONE, GDK_q, FREEWNN_KEY_HALF_ALNUM_MODE },
  { MASK_NONE, GDK_Q, FREEWNN_KEY_FULL_ALNUM_MODE },
  { MASK_CONTROL, GDK_q, FREEWNN_KEY_KANJI_MODE },
  { MASK_NONE, GDK_F3, FREEWNN_KEY_INPUT_JISCODE },
  { MASK_NONE, GDK_Muhenkan, FREEWNN_KEY_CANCEL },
  { MASK_CONTROL, GDK_c,  FREEWNN_KEY_CANCEL },
  { MASK_CONTROL, GDK_g,  FREEWNN_KEY_CANCEL },
  { MASK_CONTROL, GDK_asciicircum, FREEWNN_IDEOGRAPH_MODE },
  { MASK_NONE, 0, FREEWNN_KEY_NONE },
};

FreeWnnFuncTable eggstyle_functable[] = {
  { MASK_CONTROL, GDK_b, FREEWNN_KEY_PREV_SEGMENT },
  { MASK_CONTROL, GDK_f, FREEWNN_KEY_NEXT_SEGMENT },
  { MASK_NONE,    GDK_Left, FREEWNN_KEY_PREV_SEGMENT },
  { MASK_NONE,    GDK_Right, FREEWNN_KEY_NEXT_SEGMENT },
  { MASK_CONTROL, GDK_a, FREEWNN_KEY_FIRST_SEGNENT },
  { MASK_CONTROL, GDK_e, FREEWNN_KEY_LAST_SEGMENT },
  { MASK_CONTROL, GDK_o, FREEWNN_KEY_SEGMENT_LONGER },
  { MASK_NONE, GDK_Tab, FREEWNN_KEY_SEGMENT_SHORTER },
  { MASK_SHIFT, GDK_Right, FREEWNN_KEY_SEGMENT_LONGER },
  { MASK_SHIFT, GDK_Left, FREEWNN_KEY_SEGMENT_SHORTER },
  { MASK_ALT, GDK_s, FREEWNN_KEY_SEGMENT_ALLLIST },
  //FREEWNN_KEY_???
  { MASK_CONTROL, GDK_d, FREEWNN_KEY_DELETE_UNCOMMITTED },
  { MASK_ALT, GDK_h, FREEWNN_KEY_TO_HIRAGANA },
  { MASK_ALT, GDK_k, FREEWNN_KEY_TO_KATAKANA },
  { MASK_NONE, GDK_q, FREEWNN_KEY_HALF_ALNUM_MODE },
  { MASK_NONE, GDK_Q, FREEWNN_KEY_FULL_ALNUM_MODE },
  { MASK_CONTROL, GDK_q, FREEWNN_KEY_KANJI_MODE },
  { MASK_CONTROL, GDK_underscore, FREEWNN_KEY_INPUT_JISCODE },
  { MASK_CONTROL, GDK_c,  FREEWNN_KEY_CANCEL },
  { MASK_CONTROL, GDK_g,  FREEWNN_KEY_CANCEL },
  { MASK_CONTROL, GDK_asciicircum, FREEWNN_IDEOGRAPH_MODE },
  { MASK_NONE, 0, FREEWNN_KEY_NONE },
};

/* Default is kinput2 style */
//static FreeWnnFuncTable* functable = kinput2style_functable;

FreeWnnFuncKey
key_to_freewnn_func(FreeWnnFuncTable* functable, GdkEventKey* key) {
  FreeWnnFuncKey ret = FREEWNN_KEY_NONE;
  guint i = 0;

  /* Loop for modifier key, firstly */
  i = 0;
  while( !(functable[i].mask == MASK_NONE
    && functable[i].gdk_keycode == 0
    && functable[i].freewnn_func == FREEWNN_KEY_NONE) ) {
    if( functable[i].mask == MASK_NONE ) {
      i++;
      continue;
    }

#define MASK_MISMATCH(a) ((key->state & a) != (functable[i].mask & a))

    if( MASK_MISMATCH(GDK_CONTROL_MASK)
        || MASK_MISMATCH(GDK_SHIFT_MASK)
        || MASK_MISMATCH(GDK_MOD1_MASK) ) {
      i++;
      continue;
    }

    if( key->keyval == functable[i].gdk_keycode )
      return functable[i].freewnn_func;

    i++;
  }

  /* Loop for no modifiers key, secondly */
  i = 0;
  while( !(functable[i].mask == MASK_NONE
    && functable[i].gdk_keycode == 0
    && functable[i].freewnn_func == FREEWNN_KEY_NONE) ) {

    if( functable[i].mask != MASK_NONE ) {
      i++;
      continue;
    }

    if( key->keyval == functable[i].gdk_keycode )
      return functable[i].freewnn_func;

    i++;
  }

  return ret;
}
