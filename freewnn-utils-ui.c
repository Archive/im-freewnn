/* im-freewnn - FreeWnn immodule
 *
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "freewnn-utils-ui.h"
#include "im-freewnn-intl.h"

void
freewnn_show_warning(gchar* warntext) {
  GtkWidget* dialog = gtk_dialog_new();
  GtkWidget* label = gtk_label_new(warntext);
  GtkWidget* closebutton = gtk_button_new_from_stock("gtk-close");
  GtkWidget* warnimage = gtk_image_new_from_stock("gtk-dialog-warning", GTK_ICON_SIZE_DIALOG);
  GtkWidget* hbox = gtk_hbox_new(FALSE, 0);
                                                                              
  gtk_window_set_title(GTK_WINDOW(dialog), _("Warning"));
  gtk_box_pack_end(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox, TRUE, TRUE, 0);
                                                                              
  gtk_box_pack_start(GTK_BOX(hbox), warnimage, TRUE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  gtk_dialog_add_action_widget(GTK_DIALOG(dialog), closebutton, GTK_RESPONSE_CLOSE);
  gtk_widget_show_all(GTK_DIALOG(dialog)->vbox);
  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_hide_all(dialog);

  gtk_widget_destroy(dialog);
}

void
freewnn_show_message(gchar* text) {
  GtkWidget* dialog = gtk_dialog_new();
  GtkWidget* label = gtk_label_new(text);
  GtkWidget* closebutton = gtk_button_new_from_stock("gtk-close");
  GtkWidget* hbox = gtk_hbox_new(FALSE, 0);
                                                                              
  gtk_window_set_title(GTK_WINDOW(dialog), _("Message"));
  gtk_box_pack_end(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox, TRUE, TRUE, 0);
                                                                              
  gtk_misc_set_padding(GTK_MISC(label), 10, 10);
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
  gtk_dialog_add_action_widget(GTK_DIALOG(dialog), closebutton, GTK_RESPONSE_CLOSE);
  gtk_widget_show_all(GTK_DIALOG(dialog)->vbox);
  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_hide_all(dialog);

  gtk_widget_destroy(dialog);
}
