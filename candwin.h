/*
 * Copyright (C) 2004 Yukihiro Nakai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

/*
 * Candidate Window - candwin
 *
 * Candwin allow users to select candidate strings for the raw
 * hiragana inputs. It supports Emacs and vi style key bindings.
 *
 * Emacs style:
 *  Ctrl-f     Goto next candidate
 *  Ctrl-b     Goto previous candidate
 *  Ctrl-n     Goto next line
 *  Ctrl-p     Goto previous line
 *  Ctrl-a     Goto line top
 *  Ctrl-e     Goto line end
 *
 * Vi style:
 *  h          Goto previous candidate
 *  j          Goto next line
 *  k          Goto previous line
 *  l          Goto next candidate
 *  ^          Goto line top
 *  $          Goto line end
 *
 * NetHack Style:
 *  y          Goto up and left
 *  u          Goto up and right
 *  b          Goto down and left
 *  n          Goto down and right
 *
 * Wheel mouse:  I don't think any other doing this.
 *  Up-Scroll    Goto previous line
 *                 or goto previous candidate if there is only a line
 *  Down-Scroll  Goto next line
 *                 or goto next candidate if there is only a line
 */

#ifndef _CANDWIN_H
#define _CANDWIN_H

#include <gtk/gtk.h>

#include "im-freewnn-intl.h"

/* Extra key event callback */
typedef gboolean (*CandWinKeyPressFunc) (GdkEventKey* event, gpointer data);

typedef struct _CandWin {
  GtkWidget* window;
  GSList* cand_slist;
  gint window_width; /* Window width, based on candidates */
  gint window_height; /* Window height, based on candidates */
  gint string_maxwidth; /* Width of the most longer candidate */
  gint line_height; /* Line height */
  gint num_in_aline; /* Number of candidates in a line */
  gint num_of_lines; /* Number of lines */
  guint init_index; /* Index at start */
  guint markup_index; /* Marked up */

  CandWinKeyPressFunc extra_filter_keypress;
  gpointer keyfunc_data;
  
} CandWin;

CandWin* candwin_new();
void candwin_set_candlist(CandWin* candwin, GSList* cand_slist);
void candwin_set_index(CandWin* candwin, guint index);
guint candwin_get_index(CandWin* candwin);
void candwin_markup_up(CandWin* candwin);
void candwin_markup_down(CandWin* candwin);
void candwin_markup_right(CandWin* candwin);
void candwin_markup_left(CandWin* candwin);
void candwin_markup_linehead(CandWin* candwin);
void candwin_markup_lineend(CandWin* candwin);
void candwin_set_extra_keyfunc(CandWin* candwin, CandWinKeyPressFunc func, gpointer data);

#endif /* _CANDWIN_H */
